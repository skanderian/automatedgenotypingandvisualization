%For a set of melt curves this function plots the following:
%1) The raw fluorescence versus temperature data
%2) The Savitzky-Golay negative derivative of (interpolated) fluorescence versus temperature
%3) The temperature compensated (recalibrated) derivatives versus temperature
%4) The verically normalized derivative versus temperature within a given
%temperature range following temperature compensation
%
% written by Sami Kanderian on 12/18/2008. Canon U.S. Life Sciences Inc.

function plot_graphs(fig, prefix, D, T_eqb, all_neg_dFdT_eqb, T_eqc, all_hshift_eqc, all_vhshift_eqc, ANCKG, all_kgt, all_agt, color_by_kgt, DOPT)
p=[];
ind_replot=[103;104;105];
plotnocalls=1;
plotsingle=0;
replot=0;
assay_list=strvcat('VKORC1','CyP2C9*2','CyP2C9*3','MTHFR665');
assay_num=DOPT(1);
assay_name=assay_list(assay_num,:);
% col_mat=[0 0.75 0;
%     0 .75 1;
%     0.75 0 0;
%     0.5 0.5 0.5
%     0 0 0];
% col_mat=[0 0.75 0;
%     0 .75 1;
%     0.75 0 0;
%     0 0.75 0;
%     0 0 0];
col_mat=[0 0.75 0;
    0 .75 1;
    0.75 0 0;
    0 0.75 0;
    0.5 0.5 0.5];
% %Switch colors temporarily
% col_mat=[0.75 0 0;
%     0.5 0.5 0.5;
%     0.5 0.5 0.5;
%     0.5 0.5 0.5;
%     0.5 0.5 0.5];

SC(1).ls='--';
SC(2).ls='-';
SC(3).ls=':';
SC(4).ls='-';
SC(5).ls='-';

Temp_min_ag=T_eqc(1);
Temp_max_ag=T_eqc(end);

figure(fig);
N_class=size(ANCKG,2);

%Legend handeling
%use for legend just in case no plots exist for any of the possible
%legends)
if color_by_kgt==1
    Nleg=N_class;
else
    Nleg=N_class+1;
end
subplot(411);
p1=zeros(1,Nleg);
for j=1:Nleg
    p1(j)=plot(NaN,NaN, 'Color', col_mat(j,:));
    hold on;
end
%end of %Legend handeling

for i=1:length(all_kgt)
    clw=1;

    if color_by_kgt==1
        cgt=all_kgt(i);
    else
        cgt=all_agt(i);
    end

    if cgt==0
        cgt=4;
    end
    col_curve=col_mat(cgt,:);
    if cgt~=4 || plotnocalls==1
        subplot(411);
        title(['Curve #: ',num2str(i)]);
        p(1)= plot(D(i).T_raw, D(i).F_raw, 'Color', col_curve, 'LineWidth', clw);
        if plotsingle==0
            hold on;
        end

        subplot(412);
        p(2)=plot(T_eqb, all_neg_dFdT_eqb(:,i), 'Color', col_curve, 'LineWidth', clw);
        if plotsingle==0
            hold on;
        end

        subplot(413);
        p(3)=plot(T_eqc, all_hshift_eqc(:,i), 'Color', col_curve, 'LineWidth', clw);
        if plotsingle==0
            hold on;
        end

        subplot(414);
        p(4)=plot(T_eqc, all_vhshift_eqc(:,i), 'Color', col_curve, 'LineWidth', clw);
        if plotsingle==0
            hold on;
        else
            hold on
            for j=1:N_class
                h(j)=plot(T_eqc, ANCKG(:,j));
                set(h(j), 'LineWidth',3, 'Color', [0,0,0], 'LineStyle', SC(j).ls );
            end
            legend(h,'WT ANCKG','HE ANCKG','HM ANCKG', 'location', 'NorthWest');
            hold off
            pause;
        end
    end
    %if genotype called by algorith is known to be incorrect then plot
    %lines thicker
    %if all_agt(i)~=all_kgt(i) && all_kgt(i)~=0
    %     if i>=103 && i<=105
    %         set(p, 'LineWidth', 3, 'Color', 'b');
    %     end

    if ~isempty(p)
        p1(cgt)=p(1);
    end
end

subplot(411);
ylabel('original Fluorescence');
xlabel (' ');
%legend([p1(1),p1(2),p1(3)],'WT', 'HE', 'HM', 'location', 'NorthEast');
if color_by_kgt==1
    title([prefix,' Assay: ', assay_name,'. Colors indicate TRUE genotype']);
    legend([p1(1),p1(2),p1(3)],'WT', 'HE', 'HM', 'location', 'NorthEast');
else
    title([prefix,' Assay: ', assay_name,'. Colors indicate CALLED genotype']);
    %legend([p1(1),p1(2),p1(3),p1(4)],'WT', 'HE', 'HM', 'NC', 'location', 'NorthEast');
end
axis('tight');
Ylim=get(gca, 'Ylim');
plot([Temp_min_ag,Temp_min_ag], Ylim, 'k-', 'LineWidth', 3);
plot([Temp_max_ag,Temp_max_ag], Ylim, 'k-', 'LineWidth', 3);
Xlim=get(gca,'Xlim');

subplot(412);
ylabel('Savitzky-Golay -dF/dT');
axis('tight');
Ylim=get(gca, 'Ylim');
set(gca,'Xlim', Xlim);
plot([Temp_min_ag,Temp_min_ag], Ylim, 'k-', 'LineWidth', 3);
plot([Temp_max_ag,Temp_max_ag], Ylim, 'k-', 'LineWidth', 3);

subplot(413);
ylabel('Temperature shifted -dF/dT');
axis('tight');
Ylim=get(gca, 'Ylim');
plot([Temp_min_ag,Temp_min_ag], Ylim, 'k-', 'LineWidth', 3);
plot([Temp_max_ag,Temp_max_ag], Ylim, 'k-', 'LineWidth', 3);
subplot(414)
axis('tight');
Ylim=get(gca, 'Ylim');
plot([Temp_min_ag,Temp_min_ag], Ylim, 'k-', 'LineWidth', 3);
plot([Temp_max_ag,Temp_max_ag], Ylim, 'k-', 'LineWidth', 3);
xlabel('Temperature (\circC)');
ylabel('Normalized Shifted -dF/dT');
hold on
for j=1:N_class
    h(j)=plot(T_eqc, ANCKG(:,j));
    set(h(j), 'LineWidth',3, 'Color', [0,0,0], 'LineStyle', SC(j).ls );
end
legend(h,'WT ANCKG','HE ANCKG','HM ANCKG', 'location', 'NorthWest');
%title([prefix,' Melt curves normalized to zero mean and unit standard deviation']);
orient tall;
%print(['COVARMELT_',prefix(1),'.jpg'], '-djpeg90')
%replot
if replot==1
    for j=1:length(ind_replot)
        i=ind_replot(j);
        subplot(411);
        title(['Curve #: ',num2str(i)]);
        p(1)= plot(D(i).T_raw, D(i).F_raw, 'Color', col_curve);
        if plotsingle==0
            hold on;
        end

        subplot(412);
        p(2)=plot(T_eqb, all_neg_dFdT_eqb(:,i), 'Color', col_curve);
        if plotsingle==0
            hold on;
        end

        subplot(413);
        p(3)=plot(T_eqc, all_hshift_eqc(:,i), 'Color', col_curve);
        if plotsingle==0
            hold on;
        end

        subplot(414);
        p(4)=plot(T_eqc, all_vhshift_eqc(:,i), 'Color', col_curve);

        set(p, 'LineWidth', 1, 'Color', 'b');
    end
end
