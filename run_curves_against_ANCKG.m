function [all_corr, all_langle]=run_curves_against_ANCKG(allcurves, ANCKG)
%
%OUTPUT DIMENSIONS:
%all_corr and all_langle: [<Nmelts> x <Nclass>] where Nmelts is the number
%of melts being analyzed. Nclass is the number of possible genotypes for
%the assay. 
%
%INPUT DIMENSIONS:
%allcurves: [<Neqc> x <Nmelts>] where Neqc=round((OPT(2)-OPT(1))/OPT(3)+1);
%ANCKG: [<Neqc> x <Nclass>] where Nclass is the number of possible
%genotypes
%
%This function correlates each of a set of melt curves against each of the 
%Average Normalized Curves of Known Genotype obtained from the training
%set.
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

% ** mbfunctions for input arguments
%mbrealmatrix(allcurves)
%mbrealmatrix(ANCKG)

N_melt=size(allcurves,2);
N_class=size(ANCKG,2);
%initialize memory
all_corr=zeros(N_melt, N_class);
%calculate correlation of each melt against each of the ANCKGs
for k1=1:N_melt
    for k2=1:N_class
        all_corr(k1,k2)=mycorrcoef(allcurves(:,k1),ANCKG(:,k2));
    end
end
all_langle=langle(all_corr);