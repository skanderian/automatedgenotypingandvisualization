function [etable_wfc, err_rate_wfc, etable_wnc, err_rate_wnc, nc_rate_wnc, freq_pos_gt]=Probability_Crosstable(N_randppg,  meanclass, covclass, OPT, seed, tableswitch, ppost_mat, all_kgt)
%This function is compiled from Matlab into C using the Agility tool
%
%This function commputes an error rate probability cross table either
%through a Monte Carlo simmulation based on the distribution of the
%training set data.
%
%tableswitch:   0 training set population (Monte Carlo estimate)
%               1 validation set data
%
%Written by Sami Kanderian on July 13, 2009, Canon U.S. Life Sciences Inc.
%Last modified on 
%
%OUTPUT DIMENSIONS:
%etable_wfc: [<Nclass> x <Nclass+1>] (error table when forced calls are made)
%err_rate_wfc: [<1> x <1>] (error rate when forced calls are made)
%etable_wnc: [<Nclass> x <Nclass+1>] (error table when no calls are allowed)
%err_rate_wnc: [<1> x <1>] (error rate when no calls are allowed)
%err_rate_wnc: [<1> x <1>] (error rate when no calls are allowed)
%nc_rate_wnc: [<1> x <1>] (no call rate when no calls are allowed)
%
%INPUT DIMENSIONS:
%N_randppg_db: [<1> x <3>]
%meanclass: [<Nclass> x <Nclass>]
%covclass:[<Nclass>*<Nclass> x <Nclass>]
%ppost_mat: [<Nmelts> x <Nclass>]
%all_kgt: [<Nmelts> x <1>]
%OPT: [<1> x <17>]
%tableswitch: [<1> x <1>]
%freq_pos_gt [<1> x <Nclass>]

% ** mbfunctions for input arguments
%mbint32scalar(N_randppg)
%mbrealmatrix(meanclass)
%mbrealmatrix(covclass)
%mbrealrow(OPT)
%mbint8scalar(tableswitch)
%mbrealmatrix(ppost_mat)
%mbrealcol(all_kgt)
%mbint32scalar(seed)

N_randppg_db=double(N_randppg);
seed_db=double(seed);
tableswitch_db=double(tableswitch);

N_melts_vkgt=0;

freq_gt=OPT(14:end);
N_class=sum(freq_gt>0);

pos_gt=zeros(N_class,1);
freq_pos_gt=pos_gt;
cnt=0;
for i=1:length(OPT(14:end))
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
        freq_pos_gt(cnt)=freq_gt(i);
    end
end

if tableswitch_db==0 %do MC simulation based on training set distribution
    Class_ind=zeros(N_randppg_db*N_class,1);
    Pall=zeros(N_randppg_db*N_class, N_class);
    for k=1:N_class
        %ind_class=ind_all(all_kgt==pos_gt(k));
        %Param_mat_inclass=Param_mat(ind_class,:);
        row_st=N_class*(k-1)+1;
        row_ed=N_class*k;
        mu=meanclass(:,k);
        sigma=covclass(row_st:row_ed,:);
        if seed_db > -1
            randn('seed', seed_db);
        end
        [Prnd,T] = mymvnrnd(mu,sigma,N_randppg_db);

        ind_st=N_randppg_db*(k-1)+1;
        ind_ed=ind_st+N_randppg_db-1;

        Pall(ind_st:ind_ed,:)=Prnd;
        Class_ind(ind_st:ind_ed)=k;
    end
    [eppost_mat, epccd_mat]=calcprobability(Pall, freq_pos_gt, meanclass, covclass);
    [etable_wfc, etable_wnc]=errortablesim2(eppost_mat, Class_ind, OPT);
else %generate actual error table based on validation set
    %pick subset of only valid known genotypes
    %is_vkgt: "is valid known genotype" vector of either 1 or 0
    is_vkgt=(all_kgt>0);
    N_melts_vkgt=length(is_vkgt);
    if N_melts_vkgt>0
        ppost_mat_vkgt=ppost_mat(is_vkgt,:);
        all_kgt_vkgt=all_kgt(is_vkgt);
        eppost_mat=ppost_mat_vkgt;
        Class_ind=all_kgt_vkgt;
        [etable_wfc, etable_wnc]=errortabledat(eppost_mat, Class_ind, OPT);
        %N_melts=length(all_kgt);
    else %there are no known genotypes in validation set
        etable_wfc=zeros(N_class, N_class+1);
        etable_wnc=etable_wfc;
        err_rate_wfc=NaN;
        err_rate_wnc=NaN;
        nc_rate_wnc=NaN;
    end
end

if tableswitch_db==0 || N_melts_vkgt>0
    err_rate_wfc=0;
    err_rate_wnc=0;
    nc_rate_wnc=0;
    for i=1:N_class
        if tableswitch_db==1 %rewrite with frequency of data set otherwise stick with genotype frequency set by user
            freq_pos_gt(i)=sum(all_kgt_vkgt==i)/N_melts_vkgt;
        end
        for j=1:N_class
            if i~=j
                err_rate_wfc=err_rate_wfc+etable_wfc(i,j)*freq_pos_gt(i);
                err_rate_wnc=err_rate_wnc+etable_wnc(i,j)*freq_pos_gt(i);
            end
        end
    end
    for h=1:N_class
        nc_rate_wnc=nc_rate_wnc+etable_wnc(h,N_class+1)*freq_pos_gt(h);
    end
end

if sum(sum(etable_wfc))<1.5 %less than 2 or 3
    err_rate_wfc=NaN;
    err_rate_wnc=NaN;
    nc_rate_wnc=NaN;
end