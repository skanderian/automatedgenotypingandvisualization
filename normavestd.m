%This function performs vertical normalization of an array of curves (in
%columns). Each curve is normalizes to have a mean of 0 and a standard
%deviation of 1
%
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.
function Ymat=normavestd(Xmat)
rc=size(Xmat);
Ymat=zeros(rc);
for i=1:rc(2)
    Ymat(:,i)=(Xmat(:,i)-mean(Xmat(:,i)))/std(Xmat(:,i));
end