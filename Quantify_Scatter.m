function [S_w, S_b, dsb, dsw, qr]=Quantify_Scatter(Param_mat,Class_ind,OPT)

freq_gt=OPT(12:end);
N_class=sum(freq_gt>0);

pos_gt=zeros(N_class,1);
cnt=0;
for i=1:length(OPT(12:end))
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
    end
end



[N_melt,N_params]=size(Param_mat);
ind_all=(1:N_melt);

m=(mean(Param_mat,1))'; %N_params x 1
S_b=zeros(N_params, N_params);
S_w=zeros(N_params, N_params);
for i=1:N_class;
    ind_class=ind_all(Class_ind==pos_gt(i));
    n_i=length(ind_class);
    Param_mat_inclass=Param_mat(ind_class,:); %N_params x N_inclass
    m_i=(mean(Param_mat_inclass,1))'; %N_params x 1
    opb=n_i*(m_i-m)*(m_i-m)'; %calculate outer product
    S_b=S_b+opb;

    S_i=zeros(N_params, N_params);
    for j=1:n_i
        x=(Param_mat_inclass(j,:))'; %N_params x 1
        opi=(x-m_i)*(x-m_i)'; %N_params x N_params
        S_i=S_i+opi;
    end
    S_w=S_w + S_i;
end

S_w=S_w/N_melt;
S_b=S_b/N_melt;

dsb=det(S_b);
dsw=det(S_w);
qr=dsb/dsw;
%qr=det(S_b+S_w);
