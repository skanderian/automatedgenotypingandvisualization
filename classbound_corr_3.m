%this function plots the 3D data pooint onto a 3D graph. It enables the
%vizualization of the spread of clusters amongst different genotypes
%
% written by Sami Kanderian on 8/17/2008. Canon U.S. Life Sciences Inc.
%
function h=classbound_corr_3(P,P_ind,assay_name,col_mat)
%P(P<0)=0;
Nclass=size(P,2);
mc=0;
%points in 3D space
A=[mc,mc,mc];
B=[1,1,1];
C=[mc,1,1];
D=[mc,mc,1];
E=[1,mc,1];
F=[1,1,mc];
G=[mc,1,mc];
H=[1,mc,mc];

v=[A;B;C;D;E;F;G;H];

CL(1).f=[1,6,8
    1,8,5
    1,2,6
    1,2,5];

CL(2).f=[1,6,7
    1,2,3
    1,2,6
    1,3,7];

CL(3).f=[1,2,3
    1,2,5
    1,5,4
    1,3,4];

%figure;
hold on;
for i=1:Nclass

    CL(i).P=P((P_ind==i),:);
    if ~isempty(CL(i).P)
        if Nclass==3
            h(i)=plot3(CL(i).P(:,1), CL(i).P(:,2), CL(i).P(:,3),'.');
        else
            h(i)=plot3(CL(i).P(:,1), CL(i).P(:,2), 0*CL(i).P(:,2),'.');
        end
        set(h(i), 'color', col_mat(i,:));
    end
    patch('vertices', v, 'faces', CL(i).f, 'facecolor', col_mat(i,:), 'FaceAlpha', 0.3);

end

col_mat(5,:)=[0,0,0];
CL(i).P=P((P_ind==5),:);
if ~isempty(CL(i).P)
    if Nclass==3
        h(i)=plot3(CL(i).P(:,1), CL(i).P(:,2), CL(i).P(:,3),'.');
    else
        h(i)=plot3(CL(i).P(:,1), CL(i).P(:,2), 0*CL(i).P(:,2),'.');
    end
    set(h(i), 'color', col_mat(i,:));
end
patch('vertices', v, 'faces', CL(i).f, 'facecolor', col_mat(i,:), 'FaceAlpha', 0.3);

if Nclass==3
    title(['Assay: ',assay_name, ' Correlation distribution in 3D space'])
    xlabel('x: Correlation against WT ANCKG');
    ylabel('y: Correlation against HE ANCKG');
    zlabel('z: Correlation against HM ANCKG');
    if length(h)==3
        legend([h(1),h(2),h(3)],'WT', 'HE', 'HM', 'location', 'SouthEast');
    elseif length(h)==2
        legend([h(1),h(2)],'WT', 'HEE', 'location', 'SouthEast');
    end
    view([140,28]);
else
    title(['Assay: ',assay_name, ' Correlation distribution in 2D space'])
    xlabel('x: Correlation against WT ANCKG');
    ylabel('y: Correlation against HE ANCKG');
    %legend([h(1),h(2)],'WT', 'HE', 'location', 'SouthEast');
    view([90,0]);
end

%planes ABC and ABD
text(A(1),A(2),A(3),'A');
text(B(1),B(2),B(3),'B');
text(C(1),C(2),C(3),'C');
text(D(1),D(2),D(3),'D');
text(E(1),E(2),E(3),'E');
text(F(1),F(2),F(3),'F');
text(G(1),G(2),G(3),'G');
text(H(1),H(2),H(3),'H');


%view([-50,18]);
%view([-38,18]);