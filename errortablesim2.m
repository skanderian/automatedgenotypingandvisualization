function [etable_force_call, etable_with_nocall]=errortablesim2(ppost_mat, Class_ind, OPT)
%This function calculates the probability cross table given the calculated
%posterior probabilities 'ppost_mat', and the true genotypes listed in 'Class_ind'
%genotypes. This is used in a Monte Carlo simulation
%
%Written by Sami Kanderian on July 8, 2009. Canon U.S. Life Sciences Inc.
freq_gt=OPT(14:end);
ppost_thresh=OPT(9);

N_class=sum(freq_gt>0);

pos_gt=zeros(N_class,1);
freq_pos_gt=pos_gt;
cnt=0;
for i=1:length(OPT(14:end))
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
        freq_pos_gt(cnt)=freq_gt(i);
    end
end

N_melt=length(Class_ind);
etable_force_call=zeros(N_class, N_class+1);
etable_with_nocall=etable_force_call;

class_count=zeros(N_class,1);
for j=1:N_melt
    class_count(Class_ind(j))=class_count(Class_ind(j))+1;
    for k=1:N_class
        etable_force_call(Class_ind(j),k)=etable_force_call(Class_ind(j),k)+ppost_mat(j,k);
        if ppost_mat(j,k)>=ppost_thresh
            etable_with_nocall(Class_ind(j),k)=etable_with_nocall(Class_ind(j),k)+ppost_mat(j,k);
        else
            etable_with_nocall(Class_ind(j),N_class+1)=etable_with_nocall(Class_ind(j),N_class+1)+ppost_mat(j,k);
        end
    end
end

%force columns to add up to 1
for k=1:N_class
    etable_force_call(k,:)=etable_force_call(k,:)/class_count(k);
    etable_with_nocall(k,:)=etable_with_nocall(k,:)/class_count(k);
end