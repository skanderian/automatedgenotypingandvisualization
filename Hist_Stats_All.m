function [Ninbin, centers, data_ave, data_std, data_med, data_min, data_max]=Hist_Stats_All(data, all_gt, barwidth)
%
%OUPUT DIMENSIONS
%Ninbin: [<Nbins> x <Nugt>] where Nbins
%round(ceil(data_max/barwidth)-floor(data_min/barwidth)+1) and Nugt is the
%number of unique genotypes present
%centers: [<Nbins> x <1>]
%data_ave: [<1> x <Nugt>]
%data_std: [<1> x <Nugt>]
%data_med: [<1> x <Nugt>]
%data_min: [<1> x <Nugt>]
%data_max: [<1> x <Nugt>]
%
%INPUT DIMENSIONS:
%data: [<var> x <1>]
%barwidth: [<1> x <1>]
%all_gt: [<var> x <1>]
%
%This function generates a histogram of input data and performs statistics
%on the data.
%
% written by Sami Kanderian on 7/10/2009. Canon U.S. Life Sciences Inc.

Ndata=length(data);
[srtdata, ind]=sort(data);
all_gt_srt=all_gt(ind);
[unq_gt, junk, hist_col]=unique(all_gt_srt);
N_class=length(unq_gt);

data_all_min=srtdata(1);
fminb=floor(data_all_min/barwidth);
xmin=fminb*barwidth;
%added +barwidth/2 on 01/13/2010 to account for DLL version crashing eventhough Matlab fn
%doesn't crash
data_all_max=srtdata(Ndata)+barwidth/2;
cmaxb=ceil(data_all_max/barwidth);
xmax=cmaxb*barwidth;
%Nbins=int16(ceil(data_all_max/barwidth)-floor(data_all_min/barwidth)+1); %already
%computed portions
Nbins=round(cmaxb-fminb+1);
edges=(xmin:barwidth:xmax)';
centers=edges+barwidth/2;
Ninbin=zeros(Nbins, N_class);
binno=ones(1, N_class);
for i=1:Ndata
    while srtdata(i)>=edges(binno(hist_col(i)))+barwidth
        binno(hist_col(i))=binno(hist_col(i))+1;
    end
    Ninbin(binno(hist_col(i)),hist_col(i))=Ninbin(binno(hist_col(i)), hist_col(i))+1;
end
%set(gca,'Xlim', [xmin, xmax]);


data_ave=zeros(1, N_class);
data_std=data_ave;
data_med=data_ave;
data_min=data_ave;
data_max=data_ave;

for k=1:N_class
    srtdata_gt=srtdata(all_gt_srt==unq_gt(k));
    Ndata_gt=length(srtdata_gt);
    data_min(k)=srtdata_gt(1);
    data_max(k)=srtdata_gt(Ndata_gt);
    data_ave(k)=mean(srtdata_gt);
    data_std_tmp=std(srtdata_gt);
    data_std(k)=data_std_tmp(1,1);
    %data_med=data_median(data);
    %lines below are faster since we already sorted the data
    if mod(Ndata_gt,2)==1 %odd
        ind_data_med1=(Ndata_gt-1)/2+1;
        data_med(k)=srtdata_gt(ind_data_med1);
    else %even
        ind_data_med1=(Ndata_gt)/2;
        ind_data_med2=ind_data_med1+1;
        data_med(k)=(srtdata_gt(ind_data_med1)+srtdata_gt(ind_data_med2))/2;
    end
end