%this function plots the genotype call for each melt curve. It displays the
%correlation coefficient data as well as the posterior probability data
%
% written by Sami Kanderian on 12/18/2008. Canon U.S. Life Sciences Inc.

function plot_call(fig, prefix, Rall_kgt, Rall_agt, Rall_corr, Rall_ppost_mat, DOPT, unique_kgt)
assay_list=strvcat('VKORC1','CyP2C9*2','CyP2C9*3','MTHFR665');
assay_num=DOPT(1);
assay_name=assay_list(assay_num,:);
N_class=size(Rall_corr,2);
figure(fig);

subplot(2,1,1);
imagescbord(Rall_corr, Rall_kgt, Rall_agt, unique_kgt);
title([prefix,' Assay: ', assay_name, ' Correlation Coefficient against each of the ANCKGs'], 'FontSize', 12);
colorbar;
set(gca,'Ytick', (1:N_class));
set(gca, 'yticklabel', [' WT ANCKG'; ' HE ANCKG'; ' HM ANCKG']);

subplot(2,1,2);
imagescbord(Rall_ppost_mat, Rall_kgt, Rall_agt, unique_kgt);
title([prefix,' Assay: ', assay_name,' Posterior probability of melt curve being each of the possible genotypes'], 'FontSize', 12);
colorbar;
xlabel('melt curve #');
set(gca,'Ytick', (1:N_class));
set(gca, 'yticklabel', [' WT ANCKG'; ' HE ANCKG'; ' HM ANCKG']);