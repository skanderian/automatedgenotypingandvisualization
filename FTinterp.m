function yi=FTinterp(x,y,xi,issorted)
%FTINTERP Quick 1-D linear interpolation.
%   X must be a column vector
%   yi=FTinterp(x,y,xi) returns the value of the 1-D function Y at the
%   points XI using linear interpolation. Length(F)=length(XI).
%   The vector X specifies the coordinates of the underlying interval.
%
%   Sami Kanderian Canon US LifeSciences. Last Modified Dec 5, 2008

if issorted==0
    %The data has to be sorted by increasing temperature before fast
    %interpolation
    [x, ind]=sort(x,1);
    y=y(ind);
    %Next three lines are very important to handle repeated x (temperature) points
    ind=find(diff(x)==0);
    x(ind)=[];
    y(ind)=[];
end
ind_ed=length(x);
while isnan(x(ind_ed)) || isnan(y(ind_ed))
    ind_ed=ind_ed-1;
end

yi=interp1(x(1:ind_ed),y(1:ind_ed),xi);
%Next line added to force singe dimension
yi=yi(:);
