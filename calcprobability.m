function [ppost_mat, pccd_mat]=calcprobability(Param_mat, freq_vec, MEANCLASS, COVCLASS)
%Note: The dimensions of the outputs can be predetermined as:
%ppost_mat and pccd_mat: [<Nmelts> x <Nclass>]. 
%Nclass is the number of possible genotypes for the assay. It is calculated 
%as the number of nonzero elements from OPT(12:end). 
%Note that indexing in C is one less than Matlab
%
%For each melt curve, for each genotype, this function calculates the class conditional
%probability (pccd_mat) and the posterior probabilities (ppost_mat) from
%the frequency of each genotype in the population (freeq_vec, aka class
%priors) using an input set of parameters (Param_mat) where parameters (in
%colums) are assumed to have a Gaussian distribution according to the input
%parameters (MEANCLASS and COVCLASS)
% Written by Sami Kanderian, Canon US Life Sciences
% Inc.  Last updated on 12/11/08


[N_melt, N_param]=size(Param_mat);
N_class=size(MEANCLASS,2);
pccd_mat=zeros(N_melt, N_class);
ppost_mat=pccd_mat;
ppost_num_mat=pccd_mat;
for j=1:N_melt;
    x=Param_mat(j,:);
    for k=1:N_class
        row_st=N_param*(k-1)+1;
        row_ed=N_param*k;
        pccd_mat(j,k)=class_cond_prob(x,MEANCLASS(:,k),COVCLASS(row_st:row_ed,:));
        ppost_num_mat(j,k)=freq_vec(k)*pccd_mat(j,k);
    end
    sum_ppost=sum(ppost_num_mat(j,:));
    if sum_ppost>0 || 1
        for k=1:N_class
            ppost_mat(j,k)=ppost_num_mat(j,k)/sum_ppost;
        end
    end
end

