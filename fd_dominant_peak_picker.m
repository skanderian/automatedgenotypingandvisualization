function [T_eqp, nder_eqp, ndder_eqp, ind_pk, T_pk, sglen]=fd_dominant_peak_picker(T_sortfilp,F_Tsortfilp,dT_pk,sglen_min,sglen_max,sglen_step,sgord,T_min_pk,T_max_pk,tempshift,method)
%[T_eqp, nder_eqp, ndder_eqp, ind_pk, T_pk, sglen]        =fd_dominant_peak_picker(R(k).T_sortfilp,R(k).F_Tsortfilp,dT_pk,sglen_min,sglen_max,sglen_step,sgord,T_min_pk,T_max_pk, Rall_tempshift(k));
%fd_dominant_peak_picker picks the dominant peak of the first derivative
%-dF/dT_pk (nder_eqb) as an approximation for Tm. It does this by computing the second derivative -d2F/dT2
%(ndder_eqb). It looks for the minimum second derivative, and the maximum
%second derivatve before that point. Within that window it looks for all
%the zero crossings from positive (-d2F/dT2) to negative (-d2F/dT2). If
%there is only one zero crossing, then the temperature at that point is
%chosen as the peak. If multiple points are found then the filter window
%(strength) is increased and the process is repetead untill only one point
%is found (corresponding to the dominant peak)
%
%Inputs:
% T_sortfilp: Pre Sorted Temperature points
% F_Tsortfilp: Paired fluorescence points
% dT_pk: Temperature resolution (spacing)
% sglen_min: minimum filter length in degrees (or x units)
% sglen_min: maximum filter length in degrees (or x units)
% sglen_step: increment in filter length
% sgord: polynomial order (minimum and recomended value is 2)
%
%Outputs:
%(The length of the 3 outputed vectors is:
%[floor(T_sortfilp(end)/dT_pk)-ceil(T_sortfilp(1)/dT_pk)+1]
%
% T_eqp: Equally spaced time vector for first and second derivative
% nder_eqp: -dF/dT_pk usig SG parameters sglen and sgord
% ndder_eqp: -d2F/dT2 usig SG parameters sglen and sgord
% ind_pk: index of the dominant peak
% T_pk: temperature at the dominant peak
% slgen: resulting filter length that was necessary to find only one
% dominant peak
%
%Written by Sami Kanderian on June 30, 2009, Canon U.S. Life Sciences Inc.
%
%Last modified on October 27, 2010.

%T_sortfilp=T_sortfilp(:);
%F_Tsortfilp=F_Tsortfilp(:);

T_min_pk=ceil(T_min_pk/dT_pk)*dT_pk;
T_max_pk=floor(T_max_pk/dT_pk)*dT_pk;

%Allocate memory based on largest possible filter size
SG_winsize_deg=sglen_max;
SG_winsize_pts=(2*floor(SG_winsize_deg/dT_pk/2))+1; %guaranteed to be odd
SG_hw_pts=(SG_winsize_pts-1)/2;

T_eq_st=T_min_pk-SG_hw_pts*dT_pk;
T_eq_ed=T_max_pk+SG_hw_pts*dT_pk;


%sglen_vec=linspace(sglen_min, sglen_max, 20);
sglen_vec=(sglen_min :sglen_step: sglen_max);
N_sglen_vec=length(sglen_vec);

ind_sglen=1;
inc_fil_len=1;

T_eq=(T_eq_st: dT_pk: T_eq_ed)';
T_eqp=(T_min_pk: dT_pk: T_max_pk)';

st_ind=(T_min_pk-T_eq_st)/dT_pk+1;
ed_ind=st_ind+(T_max_pk-T_min_pk)/dT_pk;

int_st_ind=round(st_ind);
int_ed_ind=round(ed_ind);


F_eq=FTinterp(T_sortfilp+tempshift, F_Tsortfilp, T_eq,0);

while inc_fil_len==1
    sglen=sglen_vec(ind_sglen); %Error here
    SG_winsize_deg=sglen; %SG Window Size

    %Figure out true temperature bounds with extra range for shifting/filtering
    %(same for all melts)
    SG_winsize_pts=(2*floor(SG_winsize_deg/dT_pk/2))+1; %guaranteed to be odd
    %SG_hw_pts=(SG_winsize_pts-1)/2;

    %Create temperature vector with equal spacing necessary for SG analysis (same for all melts):
    %Calculate SG FIR coefficients (same for all melts)
    SGFIR_coeff=sav_golay(sgord, SG_winsize_pts);  %where ord is an integer and win_pts is an odd integer
    for k=1:sgord
        SGFIR_coeff(k+1,:)=SGFIR_coeff(k+1,:)/(dT_pk^k);
    end


    %Obtain negative derivative of fluoresence wrt temperature:
    SGFDFIR_coeff=(SGFIR_coeff(2,:))';
    SGSDFIR_coeff=(SGFIR_coeff(3,:))';
    %nder_eqp=-conv2(F_eqp, SGFDFIR_coeff, 'same');
    %Following line is the same as the above line
    nder_eq=-myconv(F_eq, SGFDFIR_coeff,1);
    ndder_eq=-myconv(F_eq, SGSDFIR_coeff,1);
    nder_eqp=nder_eq(int_st_ind:int_ed_ind);
    ndder_eqp=ndder_eq(int_st_ind:int_ed_ind);

    %Follwing two lines were substituted to handle NaNs for Agility
    [jnk1,ind_min_dd]=min(ndder_eqp);
    [jnk2,ind_max_dd]=max(ndder_eqp(1:ind_min_dd(1))); %find max ndder index before min ndder index

    ind_pk=NaN;
    T_pk=NaN;
    if method <1.5 %adaptive filter mode to find only one double derivative zero crossing
        cnt_dd_zcross=0;

        for i=ind_max_dd(1):ind_min_dd(1)
            if ndder_eqp(i)>=0 && ndder_eqp(i+1)<0
                cnt_dd_zcross=cnt_dd_zcross+1;
                %if cnt_dd_zcross==1
                if cnt_dd_zcross >0.5 && cnt_dd_zcross <1.5
                    ind_pk=i;
                else
                    ind_pk=(ind_pk*(cnt_dd_zcross-1)+i)/cnt_dd_zcross;
                end
            end
        end

        if cnt_dd_zcross>1.5
            ind_pk=round(ind_pk);
        end

        if (cnt_dd_zcross<0.5 || cnt_dd_zcross>1.5) && (ind_sglen < N_sglen_vec-0.5)%sglen~=sglen_max
            inc_fil_len=1;
            ind_sglen=ind_sglen+1;
        else
            inc_fil_len=0;
        end
    else
        inc_fil_len=0;
    end
end %while

if method>0.5  && method<2.5%peak based on max first derivative between max and min second derivatives
    [junk, ind_sub_pk]=max(nder_eqp(ind_max_dd(1):ind_min_dd(1)));
    N_max=length(ind_sub_pk);
    ind_pk=ind_sub_pk(ceil(N_max/2))+ind_max_dd(1)-1; %force to one value if multiple points have same max value and add offset for full index
elseif method>2.5 %method=3, find highest local maximum at lowest sglen setting
    N=length(T_eqp);
    max_pk=0;
    for i=2:N
        if nder_eqp(i)>=nder_eqp(i-1) && nder_eqp(i)>nder_eqp(i+1) && nder_eqp(i)>max_pk
            ind_pk=i;
            max_pk=nder_eqp(i);
        end
    end
end

if ind_pk>0
    T_pk=T_eqp(ind_pk);
end