%function sav_golay.m: written  for Master's thesis by Sami Kanderian Jr 1998.
%
%F = sav_golay(ord,wins)
%implements Savitsky-Golay smoothing.  This smoothing technique fits a polynomial
%to a moving window of data using linear least-squares fitting.  Derivatives, as
%well as the function value, can be determined at the center point.  Since the
%coefficients of the fitted polynomial are linear in the data values, we can
%precompute the filters. ord is the order of the polynomial and wins is the size
%of the moving window.
%The output F, is a matrix of dimensions ord+1 by wins. F consists of the coefficients
%of the filter. The i th row represents the coefficients used to calculate the ith derivative
%at the zero point. Each row is convoluted with the raw signal and devided by dt_daq^i to
%obtain the filtered i th derivative. dt_daq is the sampling frequency of the raw signal.

function F = sav_golay(ord,wins)


% *** check inputs ***
if  rem(wins,2)-1 ~= 0
        error('filter length is not an odd integer')
end 

% *** calculate filter coefficients ***
hw = (wins-1)/2;                    %index
X1 = [-hw:hw]'*ones(1,ord+1);  
p = ones(wins,1)*[0:ord];            % polynomial terms
X = X1.^p;                           % polynomial coefficients
F = pinv(X);                         % least squares fit pseudo-inverse invert
F = F(:,wins:-1:1);					    % flip the columns of F from left to right

for i = 0:ord							
   F(i+1,:) = F(i+1,:)*factorial(i); % multiply row i by factorial(i-1)
end

% Example: if rawdisp is the raw displacement signal with a sampling period
% of dt_daq. and the sav_golay filter is to be used to ontain disp, the filtered
% version of rawdisp, and we also wish to obtain the velociity and acceleration
% signals, vel, and acc then from the command line type:

%sav=sav_golay(ord,win);  where ord is an integer and win is an odd integer

%sd0 = sav(1,:)';
%sd1 = sav(2,:)'/dt_daq;
%sd2 = sav(3,:)'/(dt_daq*dt_daq);


%disp=conv2(counts, sd0, 'same');
%vel=conv2(counts, sd1, 'same');
%acc=conv2(counts, sd2, 'same');
