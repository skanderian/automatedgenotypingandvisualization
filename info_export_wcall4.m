%info_export_wcall3(RD, VD, Rall_tempshift, Vall_tempshift, Rall_corr, Vall_corr, Rall_langle, Vall_langle, Rall_ppost_mat, Vall_ppost_mat, Rall_agt, Vall_agt, dname_out, fout_suffix, OPT, assayName);
function info_export_wcall4(TD, VD, TF_Tshift, VF_Tshift, TCmat, VCmat, TLAmat, VLAmat, Tppostmat, Vppostmat, Tagt, Vagt, RTmmat, VTmmat, dname_out, fout_suffix, OPT, assayName)

%1: VKORC1
%2: CyP2C9*2
%3: CyP2C9*3
%COAGULATION
%4: MTHFR665


fname_out=['Output_',fout_suffix];

fid=fopen([dname_out,fname_out,'.csv'],'w');
fprintf(fid,'%s,%s \n','Assay: ', assayName);
fprintf(fid,'%s,%d \n','Tmin: ', OPT(1));
fprintf(fid,'%s,%d \n','Tmax: ', OPT(2));
fprintf(fid,'%s \n',' ');

keyGenotype=['WT';'HE';'HM'];



fprintf(fid,'%s \n','TRAINING FILES:');
fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s \n','Index','File Name ','Header', 'Location', 'Temp Shift', 'Corr_WT', 'Corr_HE', 'Corr_HM', 'Length', 'Angle1', 'Angle2', 'ppost_WT', 'ppost_HE', 'ppost_HM','True Genotype', 'Automated Call Genotype', 'Unshifted Tm');

for i=1:length(TD);
    calledGT='UK';
    if Tagt(i)>0;
        calledGT=keyGenotype(Tagt(i),:);
    end
    fprintf(fid,'%d,%s,%s,%s,%3.3f,%3.5f,%3.5f,%3.5f,%3.5f,%3.5f,%3.5f,%1.8f,%1.8f,%1.8f,%s,%s,%3.5f \n',i,TD(i).fname, TD(i).header, TD(i).location, TF_Tshift(i), TCmat(i,1), TCmat(i,2), TCmat(i,3), TLAmat(i,1), TLAmat(i,2), TLAmat(i,3), Tppostmat(i,1), Tppostmat(i,2), Tppostmat(i,3), TD(i).kgtname, calledGT, RTmmat(i));
    %disp('wait here');
end
fprintf(fid,'%s \n',' ');

if ~isempty(VD)
    fprintf(fid,'%s \n','VALIDATION FILES:');
    fprintf(fid,'%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s \n','Index','File Name ','Header', 'Location', 'Temp Shift', 'Corr_WT', 'Corr_HE', 'Corr_HM', 'Length', 'Angle1', 'Angle2', 'ppost_WT', 'ppost_HE', 'ppost_HM','True Genotype', 'Automated Call Genotype', 'Unshifted Tm');

    for i=1:length(VD);
        calledGT='UK';
        if Vagt(i)>0;
            calledGT=keyGenotype(Vagt(i),:);
        end
        %fprintf(fid,'%d,%s,%s,%s,%3.3f,%3.5f,%3.5f,%3.5f,%s,%s \n',i,VD(i).fname, VD(i).header, VD(i).location, VF_Tshift(i), VCmat(i,1), VCmat(i,2), VCmat(i,3), VD(i).kgtname, calledGT);
        fprintf(fid,'%d,%s,%s,%s,%3.3f,%3.5f,%3.5f,%3.5f,%3.5f,%3.5f,%3.5f,%1.8f,%1.8f,%1.8f,%s,%s,%3.5f \n',i,VD(i).fname, VD(i).header, VD(i).location, VF_Tshift(i), VCmat(i,1), VCmat(i,2), VCmat(i,3), VLAmat(i,1), VLAmat(i,2), VLAmat(i,3), Vppostmat(i,1), Vppostmat(i,2), Vppostmat(i,3), VD(i).kgtname, calledGT, VTmmat(i));
    end
    fprintf(fid,'%s \n',' ');
end

fclose(fid);
