# README #

This MATLAB code implements the method describe in the PLOS ONE article: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0143295
The main script that executes all subsequent functions is “main_melt_app.m”
Input Melt files are located in the folder: \MeltData
Output results are located in the folder: \AutomatedResults

Sami Kanderian