function [all_corr2, triaxes2, meanclass2, covclass2, maxeval2, mineval2, ang2]=Scatter_Analysis_2D(all_corr, all_kgt, OPT)
%
%This function does 2D scatter analysis following dimensionality reduction
%from 3D to 2D via projection of the 3D points to the plane normal to the (1,1,1) vector
%
%Written by Sami Kanderian on May 27, 2009
%OUTPUT DIMENSIONS:
%all_corr2: [<Nmelts> x <2>]
%triaxes2: [<3> x <2>]
%meanclass2: [<2> x <3>]
%covclass2:[<6> x <2>]
%maxeval2:[<3> x <1>]
%mineval2:[<3> x <1>]
%ang2:[<3> x <1>]
%
%INPUT DIMENSIONS:
%all_corr: [<Nmelts> x <3>]
%all_kgt: [<Nmelts> x <1>]
%OPT: [<1> x <15>] OPT is different only for this one DLL to pick out
%correct ROWS for 2D ellipse generation. freqs of all but 3 genotypes are
%zeroed out

%Perform dimensional reduction:
all_corr2=Dim_Reduction(all_corr);

%Obtain transformed axis (C,F,E) scaled to data
maxlp2=max((all_corr2(:,1).^2 + all_corr2(:,2).^2).^0.5);
axes=[0,1,1; 1,1,0; 1,0,1];
transaxes=Dim_Reduction(axes);
maxla2=max((transaxes(:,1).^2 + transaxes(:,2).^2).^0.5);
triaxes2=maxlp2/maxla2*transaxes;

freq_gt=OPT(14:end);
%N_class=sum(freq_gt>0);
N_class=3; %force to 3
pos_gt=zeros(N_class,1);
cnt=0;
for i=1:length(OPT(14:end))
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
    end
end

[N_melt]=size(all_corr,1);
ind_all=(1:N_melt);

meanclass2=zeros(2, N_class);
covclass2=zeros(2*N_class, 2);
maxeval2=zeros(1,N_class);
mineval2=maxeval2;
ang2=maxeval2;

for j=1:N_class
    ind_class=ind_all(all_kgt==pos_gt(j));
    row_st=2*(j-1)+1;
    row_ed=2*j;
    all_corr2_inclass=all_corr2(ind_class,:);
    M=(mean(all_corr2_inclass,1))';
    meanclass2(:,j)=M;
    C=mycov(all_corr2_inclass);
    covclass2(row_st:row_ed,:)=C;
    [Tevec,Teval]=eig(C); %best
    Teval=real(Teval);
    if Teval(2,2)>Teval(1,1)
        %make sure eigenvector corresponding to largest eigenvalue appears
        %first
        R=[Tevec(:,2), Tevec(:,1)];
        maxeval2(j)=Teval(2,2);
        mineval2(j)=Teval(1,1);
    else
        R=Tevec;
        maxeval2(j)=Teval(1,1);
        mineval2(j)=Teval(2,2);
    end
    acr11=acos(R(1,1));
    asr12=asin(R(1,2));
    if round((acr11+asr12)*180/pi)==180
        %R must be negated
        %ang2(j)=acos(-R(1,1));
        %following line is equivelant but does not require additional acos
        %computation
        ang2(j)=(pi-acr11);
    else
        ang2(j)=acr11;
    end
end
