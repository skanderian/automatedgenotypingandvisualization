
%% This script file is the main matlab script that runs the Melt Software
%  written by Sami Kanderian on December 10, 2008,  Canon U. S. Life Sciences Inc.
%  last modified on February 18, 2008
askplot=1; askcall=1; onlytwo=0;
tocvec=zeros(23,1);
Tt_divisor=1;
FTpaircheck=int8(0);
%% INPUT OPTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Input file specifications
assay_num=1; %1 to 4
assayList{1}='VKORC1';
assayList{2}='CyP2C92';
assayList{3}='CyP2C93';
assayList{4}='MTHFR665';

assayName=assayList{assay_num};

%WARFARIN
%1: VKORC1
%2: CyP2C9*2
%3: CyP2C9*3
%COAGULATION
%4: MTHFR665

if assay_num==1    
    Rdname_in=['.\MeltData\Training\'];
    Vdname_in=['.\MeltData\Validation\'];
    Rfnames_in=dir([Rdname_in,'*VKORC1*.txt']);
    Vfnames_in=dir([Vdname_in,'*VKORC1*.txt']);    
elseif assay_num==2
    Rdname_in=['.\MeltData\Training\'];
    Vdname_in=['.\MeltData\Validation\'];
    Rfnames_in=dir([Rdname_in,'*C92*.txt']);
    Vfnames_in=dir([Vdname_in,'*C92*.txt']);    
elseif assay_num==3
    Rdname_in=['C:\MeltData\Warfarin_Small_Amplicon\GP2\'];
    Vdname_in=['C:\MeltData\Warfarin_Small_Amplicon\GP3\'];
    Rfnames_in=dir([Rdname_in,'*c93*.txt']);
    Vfnames_in=dir([Vdname_in,'*c93*.txt']);
    indThick=[];
elseif assay_num==4
    Rdname_in=['C:\MeltData\Lingxia\MTHFR665\09092015\'];
    Vdname_in=['C:\MeltData\Lingxia\MTHFR665\09092015\'];
    Rfnames_in=dir([Rdname_in,'MTHFR665_A.txt']);
    Vfnames_in=dir([Vdname_in,'*_UK.txt']);    
end

dname_out=['.\AutomatedResults\'];
fout_suffix=[datestr(now,29),'_',assayName];

%machtype_num
machtype_num=1;
%1: LC480;
%2: Caliper
machtype_names=['LC480  '; 'Caliper'];
opt_dfname=['.\default_options_',deblank(machtype_names(machtype_num,:)),'.csv'];
%read default options from appropriate file;
[OPT]=get_default_options(opt_dfname, assay_num);
DOPT(1)=assay_num;
DOPT(2)=machtype_num;

%cd(dname_out);

%% TRAINING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Input Data Files
Rexclude_ind=[];
KEY=[];
[RD,RF,RNdiec,RT_min_data,RT_max_data]=import_file_data(Rdname_in, Rfnames_in, Rexclude_ind, DOPT, KEY);
N_RD=length(RD);

%STEP R1: Get SG coefficients and T_eq based on training data
tic;
[RT_eqa, RT_eqb, SGFIR_coeff]=get_T_eq_and_SGFIR(OPT, RT_min_data, RT_max_data);
tocvec(1)=toc;
disp(['Elapsed time for ''get_T_eq_and_SGFIR'' is: ', num2str(tocvec(1))])
N_R_eqa=length(RT_eqa);
N_R_eqb=length(RT_eqb);
%initialize memory for variables
Rall_F_eqa=zeros(N_R_eqa, N_RD);
Rall_neg_dFdT_eqb=zeros(N_R_eqb, N_RD);
Rall_kgt=zeros(N_RD,1);
Rall_loc=zeros(N_RD,2);
Rall_expno=zeros(N_RD,1);
Rall_T_pk=zeros(N_RD,1);
%STEP R2: Generate interpolated fluorescence and Savitzky-Golay derivative and store
%results in arrays (TRAINING SET)
tocvec(2)=toc;
for k=1:N_RD
    [T_fil, T_filp, T_sortfilp, F_Tsortfilp]=Preprocessing(RD(k).time, RD(k).T_raw, RD(k).time, RD(k).F_raw, Tt_divisor, OPT, FTpaircheck);
    [F_eqa, neg_dFdT_eqb]= SG_Interp_Fluorescence(T_sortfilp, F_Tsortfilp, RT_eqa, SGFIR_coeff);
    %Collect melt variables of interest into arrays
    Rall_F_eqa(:,k)=F_eqa;
    Rall_neg_dFdT_eqb(:,k)=neg_dFdT_eqb;
    Rall_kgt(k)=RD(k).kgt;
    Rall_loc(k,1)=RD(k).ylocation;
    Rall_loc(k,2)=RD(k).xlocation;
    Rall_expno(k)=RD(k).fnum;    
    R(k).T_sortfilp=T_sortfilp;
    R(k).F_Tsortfilp=F_Tsortfilp;
end
tocvec(3)=toc;
difftoc=tocvec(3)-tocvec(2);
disp(['Elapsed time for ''SG_Interp_Fluorescence'' is: ', num2str(difftoc), ' for ', num2str(N_RD), ' melt curves.']);


%STEP R3: Perform Horizontal (Temperature) shifting and return subset of interest
%Note: For training set, 'inrefcurve' is a vector of zeros;
Np_inrefcurve=round((OPT(2)-OPT(1))/OPT(3)+1);
inrefcurve=zeros(Np_inrefcurve,1);
tocvec(4)=toc;
[T_eqc, refcurve, Rall_hshift_eqc, Rall_tempshift]=Horizontal_Shift_Subset(RT_eqb, Rall_neg_dFdT_eqb, Rall_expno, Rall_kgt, OPT, inrefcurve);


tocvec(5)=toc;
difftoc=tocvec(5)-tocvec(4);
disp(['Elapsed time for ''Horizontal_Shift_Subset'' is: ', num2str(difftoc), ' for ', num2str(N_RD), ' melt curves.']);

%STEP R4: Perform Vertical (Fluorescence) Normalization:
tocvec(6)=toc;

Rall_vhshift_eqc=Vertical_Normalization(Rall_hshift_eqc);
tocvec(7)=toc;
difftoc=tocvec(7)-tocvec(6);
disp(['Elapsed time for ''Vertical_Normalization'' is: ', num2str(difftoc), ' for ', num2str(N_RD), ' melt curves.']);

if askcall==1
    %STEP R5: Perform AI Machine learning or Training of Data
    tocvec(8)=toc;
    [ANCKG, SNCKG, meanclass, covclass, Rall_corr, Rall_langle]=AI_Training(Rall_vhshift_eqc, Rall_kgt, OPT);
    
    %Quantify Scatter in dimensions eqaual to number of classes or genotypes
    [dsb_nc, dsw_nc, qr_nc]=Quantify_Scatter(Rall_langle,Rall_kgt,OPT);

    tocvec(9)=toc;
    difftoc=tocvec(9)-tocvec(8);
    disp(['Elapsed time for ''AI_Training'' is: ', num2str(difftoc), ' for ', num2str(N_RD), ' melt curves.']);
    %STEP R5b: 2D scatter analysis of training data
    Rall_corr_b=Rall_corr;
    Rall_kgt_b=Rall_kgt;
    if onlytwo==1
        Rall_corr_b(:,3)=0;
        ind_rem=find(Rall_kgt==3);
        Rall_corr_b(ind_rem,:)=[];
        Rall_kgt_b(ind_rem,:)=[];
    end
    [Rall_corr2, Rtriaxes2, meanclass2, covclass2, maxeval2, mineval2, ang2]=Scatter_Analysis_2D(Rall_corr_b, Rall_kgt_b, OPT);
    %Quantify Scatter in 2 dimensions
    %[dsb_2, dsw_2, qr_2]=Quantify_Scatter(Rall_corr2,Rall_kgt,OPT);

    Nppe=100; %Draw ellipse with 100 points
    numstdev=3; %number of standard deviations that is used to compute lengths of major and minor axes of ellipses.
    %STEP R5c: Generate ellipses from training data
    
    [ellipse_x, ellipse_y, percontainpop, axes_x, axes_y]=Generate_Ellipses_2D_waxis(meanclass2, maxeval2, mineval2, ang2, Nppe, numstdev, OPT);

    %STEP R6: Generate call (Self-Validation of training set. Self Validation
    %not necessary in the MeltApp). i.e Step V6 is necessary but not Step R6.
    tocvec(10)=toc;
    [Rall_agt, Rall_ppost_mat, Rall_pccd_mat]=Call_Genotype(Rall_corr, Rall_langle, meanclass, covclass, OPT);
       
    tocvec(11)=toc;
    difftoc=tocvec(11)-tocvec(10);
    disp(['Elapsed time for ''Call_Genotype'' is: ', num2str(difftoc), ' for ', num2str(N_RD), ' melt curves.']);
    Rdfname_out=[Vdname_in,'Results\Self_Validation.csv'];
end

%Peak picker on training set
%Calculate Temperature peak of first derivative of melt cure (prior to
%any shifting) to estimate Tm. (THIS STEP IS NOT REQUIRED OR USED FOR
%GENOTYPING, BUT IS ONLY USED FOR ADDITIONAL INFORMATION)
dT_pk=0.01; sglen_min=1; sglen_max=5; sglen_step=0.2; sgord=2; T_min_pk=80; T_max_pk=90;
Rall_T_pk=zeros(N_RD,1);
for k=1:N_RD
    [T_eqp, nder_eqp, ndder_eqp, ind_pk, T_pk, sglen]=fd_dominant_peak_picker(R(k).T_sortfilp,R(k).F_Tsortfilp,dT_pk,sglen_min,sglen_max,sglen_step,sgord,T_min_pk,T_max_pk, 0*Rall_tempshift(k),1);    
    %Collect melt variables of interest into arrays
    Rall_T_pk(k)=T_pk;
end
%End of Tm calculation loop

data=Rall_T_pk(:);
data=data(:);
barwidth=1;
ispercent=int8(0);

[Ninbin, centers, data_ave, data_std, data_med, data_min, data_max]=Hist_Stats_All(data, Rall_kgt, barwidth);
N_randppg=int32(100);%10000
tableswitch=int8(0); % 0 --> MC, 1--> Validation
seed=int32(0);
[etable_wfc, err_rate_wfc, etable_wnc, err_rate_wnc, nc_rate_wnc, freq_pos_gt]=Probability_Crosstable(N_randppg, meanclass, covclass, OPT, seed, tableswitch, Rall_ppost_mat, Rall_kgt);

if askplot
    unique_kgt=sort(unique(Rall_kgt));
    %Plot training results where colors denote TRUE genotype
    plot_graphs(1, '-TRAINING- ', RD, RT_eqb, Rall_neg_dFdT_eqb, T_eqc, Rall_hshift_eqc, Rall_vhshift_eqc, ANCKG, Rall_kgt, Rall_agt,1,DOPT);
    print([dname_out, 'Training_Fig1_', fout_suffix],'-dtiff','-r600');
    %Plot training results where colors denote CALLED genotype
    plot_graphs(2, '-SELF VALIDATION- ', RD, RT_eqb, Rall_neg_dFdT_eqb, T_eqc, Rall_hshift_eqc, Rall_vhshift_eqc, ANCKG, Rall_kgt, Rall_agt,0,DOPT);
    print([dname_out, 'Training_Fig2_', fout_suffix],'-dtiff','-r600');
    plot_distribution(3, '-SELF VALIDATION- ', Rall_kgt, Rall_corr, Rall_langle, DOPT);
    print([dname_out, 'Training_Fig3_', fout_suffix],'-dtiff','-r600');
    plot_call(4, '-SELF VALIDATION- ', Rall_kgt, Rall_agt, Rall_corr, Rall_ppost_mat, DOPT, unique_kgt);
    print([dname_out, 'Training_Fig4_', fout_suffix],'-dtiff','-r600');
    %STEP R5c: Draw ellipses from training data
    figure(5);    
    Plot_Ellipses_waxis(ellipse_x, ellipse_y, OPT, Nppe, axes_x, axes_y);
    ttl{1}=['Ellipses represent training set data distribution'];
    %STEP R5b: 2D Draw points from training data where colors indicate
    %known genotype (kgt)
    Plot_Points_2D(Rall_corr2, Rall_kgt_b, OPT)
    ttl{2}=['Each point represent a melt curve in the training set whose color denotes the known genotype'];
    title(ttl);
    xlabel('Transformed 2D x-axis');
    ylabel('Transformed 2D y-axis');
    print([dname_out, 'Training_Fig5_', fout_suffix],'-dtiff','-r600');
end
%% VALIDATION/UNKOWN DNA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Input Data Files
Vexclude_ind=[];
KEY=[];
[VD,VF,VNdiec,VT_min_data,VT_max_data]=import_file_data(Vdname_in, Vfnames_in, Vexclude_ind, DOPT, KEY);
N_VD=length(VD);

%STEP V1: Get SG coefficients and T_eq based on training data
tocvec(12)=toc;
[VT_eqa, VT_eqb, SGFIR_coeff]=get_T_eq_and_SGFIR(OPT, VT_min_data, VT_max_data);
tocvec(13)=toc;
difftoc=tocvec(13)-tocvec(12);
disp(['Elapsed time for ''get_T_eq_and_SGFIR'' is: ', num2str(difftoc)])

N_V_eqa=length(VT_eqa);
N_V_eqb=length(VT_eqb);
%initialize memory for variables
Vall_F_eqa=zeros(N_V_eqa, N_VD);
Vall_neg_dFdT_eqb=zeros(N_V_eqb, N_VD);
Vall_kgt=zeros(N_VD,1);
Vall_loc=zeros(N_VD,2);
Vall_expno=zeros(N_VD,1);
%STEP V2: Generate interpolated fluorescence and Savitzky-Golay derivative and store
%results in arrays (TRAINING SET)
tocvec(14)=toc-tocvec(13);
for k=1:N_VD
    [T_fil, T_filp, T_sortfilp, F_Tsortfilp]=Preprocessing(VD(k).time, VD(k).T_raw, VD(k).time, VD(k).F_raw, Tt_divisor, OPT, FTpaircheck);
    [F_eqa, neg_dFdT_eqb]= SG_Interp_Fluorescence(T_sortfilp, F_Tsortfilp, VT_eqa, SGFIR_coeff);
    %Collect melt variables of interest into arrays
    Vall_F_eqa(:,k)=F_eqa;
    Vall_neg_dFdT_eqb(:,k)=neg_dFdT_eqb;
    Vall_kgt(k)=VD(k).kgt;
    Vall_loc(k,1)=VD(k).ylocation;
    Vall_loc(k,2)=VD(k).xlocation;
    Vall_expno(k)=VD(k).fnum;
    V(k).T_sortfilp=T_sortfilp;
    V(k).F_Tsortfilp=F_Tsortfilp;
end
tocvec(15)=toc;
difftoc=tocvec(15)-tocvec(14);
disp(['Elapsed time for ''SG_Interp_Fluorescence'' is: ', num2str(difftoc), ' for ', num2str(N_VD), ' melt curves.']);

%STEP V3: Perform Horizontal (Temperature) shifting and return subset of interest
%Note: For training set, 'inrefcurve' is a vector of zeros, if validation then 'inrefcurve' is set to 'refcurve' outputed from 'Horizontal_Shift_Subset' of training set;
inrefcurve=refcurve;
tocvec(16)=toc;
[T_eqc, refcurve, Vall_hshift_eqc, Vall_tempshift]=Horizontal_Shift_Subset(VT_eqb, Vall_neg_dFdT_eqb, Vall_expno, Vall_kgt, OPT, inrefcurve);

tocvec(17)=toc;
difftoc=tocvec(17)-tocvec(16);
disp(['Elapsed time for ''Horizontal_Shift_Subset'' is: ', num2str(difftoc), ' for ', num2str(N_VD), ' melt curves.']);

%STEP V4: Perform Vertical (Fluorescence) Normalization:
tocvec(18)=toc;
Vall_vhshift_eqc=Vertical_Normalization(Vall_hshift_eqc);
tocvec(19)=toc;
difftoc=tocvec(19)-tocvec(18);
disp(['Elapsed time for ''Vertical_Normalization'' is: ', num2str(difftoc), ' for ', num2str(N_VD), ' melt curves.']);

%STEP V5: Calculate pattern matching parameters of all curves of unknown genotype
%against the ANCKG curves
tocvec(20)=toc;
[Vall_corr, Vall_langle]=run_curves_against_ANCKG(Vall_vhshift_eqc, ANCKG);
tocvec(21)=toc;
difftoc=tocvec(21)-tocvec(20);
disp(['Elapsed time for ''run_curves_against_ANCKG'' is: ', num2str(difftoc), ' for ', num2str(N_VD), ' melt curves.']);

%STEP V6: Generate call (probability)
tocvec(22)=toc;
[Vall_agt, Vall_ppost_mat, Vall_pccd_mat]=Call_Genotype(Vall_corr, Vall_langle, meanclass, covclass, OPT);
tocvec(23)=toc;
difftoc=tocvec(23)-tocvec(22);
disp(['Elapsed time for ''Call_Genotype'' is: ', num2str(difftoc), ' for ', num2str(N_VD), ' melt curves.']);

%Calculate Temperature peak of first derivative of melt cure (prior to
%any shifting) to estimate Tm. (THIS STEP IS NOT REQUIRED OR USED FOR
%GENOTYPING, BUT IS ONLY USED FOR ADDITIONAL INFORMATION)
%Peak picker on validation set
dT_pk=0.01; sglen_min=0.6; sglen_max=3.4; sglen_step=0.2; sgord=2; T_min_pk=70; T_max_pk=90;
lmat=zeros(N_VD,3);
Vall_T_pk=zeros(N_VD,1);
for k=1:N_VD    
    [T_eqp, nder_eqp, ndder_eqp, ind_pk, T_pk, sglen]=fd_dominant_peak_picker(V(k).T_sortfilp,V(k).F_Tsortfilp,dT_pk,sglen_min,sglen_max,sglen_step,sgord,T_min_pk,T_max_pk, 0*Vall_tempshift(k),1);
    %Collect melt variables of interest into arrays
    Vall_T_pk(k)=T_pk;
    lmat(k,1)=length(T_eqp);
    lmat(k,2)=length(nder_eqp);
    lmat(k,3)=length(ndder_eqp);
end
%End of Tm calculation loop

[Vall_corr2, Vtriaxes2, Vmeanclass2, Vcovclass2, Vmaxeval2, Vmineval2, Vang2]=Scatter_Analysis_2D(Vall_corr, Vall_agt, OPT);
if askplot
    %Plot validation results where colors denote TRUE genotype
    plot_graphs(6, '-VALIDATION- ', VD, VT_eqb, Vall_neg_dFdT_eqb, T_eqc, Vall_hshift_eqc, Vall_vhshift_eqc, ANCKG, Vall_kgt, Vall_agt,1,DOPT);
    print([dname_out, 'Validation_Fig6_', fout_suffix],'-dtiff','-r600');
    %Plot validation results where colors denote CALLED genotype
    plot_graphs(7, '-VALIDATION- ', VD, VT_eqb, Vall_neg_dFdT_eqb, T_eqc, Vall_hshift_eqc, Vall_vhshift_eqc, ANCKG, Vall_kgt, Vall_agt,0,DOPT);
    print([dname_out, 'Validation_Fig7_', fout_suffix],'-dtiff','-r600');
    
    plot_distribution(8, '-CROSS VALIDATION- ', Vall_agt, Vall_corr, Vall_langle, DOPT); %color of points plotted are called genotype
    print([dname_out, 'Validation_Fig8_', fout_suffix],'-dtiff','-r600');
    plot_call(9, '-VALIDATION- ', Vall_kgt, Vall_agt, Vall_corr, Vall_ppost_mat, DOPT, unique_kgt);
    print([dname_out, 'Validation_Fig9_', fout_suffix],'-dtiff','-r600');
    figure(10);
    %STEP V5c: Draw ellipses from training data
    Plot_Ellipses_waxis(ellipse_x, ellipse_y, OPT, Nppe, axes_x, axes_y);
    ttl{1}=['Ellipses represent training set data distribution'];
    %STEP V5b: 2D Draw points from validation data where colors indicate
    %algorithm called genotype (agt):
    Plot_Points_2D(Vall_corr2, Vall_agt, OPT); %agt: automated genotype, kgt: known genotype
    ttl{2}=['Each point represent a melt curve in the validation set whose color denotes the called genotype'];
    title(ttl);
    xlabel('Transformed 2D x-axis');
    ylabel('Transformed 2D y-axis');
    print([dname_out, 'Validation_Fig10_', fout_suffix],'-dtiff','-r600');
end
disp(['Total elapsed time is ',num2str(toc), ' seconds.']);

info_export_wcall4(RD, VD, Rall_tempshift, Vall_tempshift, Rall_corr, Vall_corr, Rall_langle, Vall_langle, Rall_ppost_mat, Vall_ppost_mat, Rall_agt, Vall_agt, Rall_T_pk, Vall_T_pk, dname_out, fout_suffix, OPT, assayName);



%% NOTE The similarities and differences in procedure between TRAINING and VALIDATION
%
%STEP V1 is similar to STEP R1
%
%STEP V2 is similar to STEP R2
%
%STEP V3 is similar to STEP R3 but :
%the 'inrefcurve' input for STEP R3 is a vector of zeros;
%but  the 'inrefcurve' input for STEP R3 is set to 'refcurve' outputed from 'Horizontal_Shift_Subset' called using training set;
%
%STEP V4 is similar to STEP R4
%
%STEP V5 is different than STEP R5 a :s
%STEP R5 involves AI Learning generated from the training set, where self pattern matching wrt the gererated ANCKG curves happens
%within.
%STEP V5 involves pattern matching of the DNA melt curves of unknown
%genotype with the ANCKG generated from STEP R5
%
%STEP V6 is similar to STEP R6