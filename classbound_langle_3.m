function h=classbound_langle_3(P,P_ind,assay_name,col_mat)
%P(P<0)=0;
Nclass=size(P,2);

%points in 3D space
A=[0,0,0];
B=[1,1,1];
C=[0,1,1];
D=[0,0,1];
E=[1,0,1];
F=[1,1,0];
G=[0,1,0];
H=[1,0,0];

v=[A;B;C;D;E;F;G;H];

CL(1).f=[1,6,8
    1,8,5
    1,2,6
    1,2,5];

CL(2).f=[1,6,7
    1,2,3
    1,2,6
    1,3,7];

CL(3).f=[1,2,3
    1,2,5
    1,5,4
    1,3,4];

%figure;
hold on;
view([140,28]);
for i=1:Nclass

    CL(i).P=P((P_ind==i),:);
    if ~isempty(CL(i).P)
        if Nclass==3
            h(i)=plot3(CL(i).P(:,1), CL(i).P(:,2), CL(i).P(:,3),'.');
        else
            h(i)=plot3(CL(i).P(:,1), CL(i).P(:,2), 0*CL(i).P(:,2),'.');
        end
        set(h(i), 'color', col_mat(i,:));
    end
    %patch('vertices', v, 'faces', CL(i).f, 'facecolor', col_mat(i,:), 'FaceAlpha', 0.3);

end
view([140,28]);
% title('Go');
% cnt=0;
% for i=1:3:87;
%     if i==1
%         h=plot3(P(i:i+2,1), P(i:i+2,2), P(i:i+2,3),'.');
%         set(h, 'color', [0,0,0],'MarkerSize',14);
%     else
% 
%         set(h, 'XData', P(i:i+2,1));
%         set(h, 'YData', P(i:i+2,2));
%         set(h, 'ZData', P(i:i+2,3));
%     end
%     cnt=cnt+1;
%     title(['Unknown Sample: ',num2str(cnt)]);
%     pause;
% end
% cnt=0;
% col_mat=[0.75 0 0
%     0 0.75 1
%     0 0.75 0];
% TS_mat=['HM';'HE';'WT'];
% for i=88:2:93;
%     cnt=cnt+1;
%     h=plot3(P(i:i+1,1), P(i:i+1,2), P(i:i+1,3),'o');
%     set(h, 'color', col_mat(cnt,:),'MarkerSize',18);
%     title(['Training Sample: ', TS_mat(cnt,:)]);
%     pause;
% end
% title('DONE');
% pause
if Nclass==3
    title(['Assay: ',assay_name, ' Normalized parameter distribution in 3D space'])
    xlabel('x: Length');
    ylabel('y: Angle 1');
    zlabel('z: Angle 2');
    if length(h)==3
        legend([h(1),h(2),h(3)],'WT', 'HE', 'HM', 'location', 'NorthWest');
    elseif length(h)==2
        legend([h(1),h(2)],'WT', 'HE', 'location', 'NorthWest');
    end
    view([140,28]);
else
    title(['Assay: ',assay_name, '  Normalized parameter distribution in 2D space'])
    xlabel('x: Length');
    ylabel('y: Angle 1');
    %legend([h(1),h(2)],'WT', 'HE', 'location', 'NorthWest');
    view([90,0]);
end

%planes ABC and ABD
% text(A(1),A(2),A(3),'A');
% text(B(1),B(2),B(3),'B');
% text(C(1),C(2),C(3),'C');
% text(D(1),D(2),D(3),'D');
% text(E(1),E(2),E(3),'E');
% text(F(1),F(2),F(3),'F');
% text(G(1),G(2),G(3),'G');
% text(H(1),H(2),H(3),'H');


%view([-50,18]);
%view([-38,18]);