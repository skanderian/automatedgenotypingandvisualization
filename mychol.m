function T=mychol(S,isupper)
%function T=mychol(S,isupper)
%This finction performs Cholesky decomposition is a decomposition of a symmetric,
%positive-definite matrix, S into the product of a lower triangular matrix and its conjugate transpose upper triangular matrix. 
%if isupper =1, then the upper matrix is returned as T, otherwize the lower
%matrix is returned. It does not check to see if S is a positive-definite,
%as its use is meant on covariance matrices which are guaranteed to be
%positive-definite as well as symetric. But the check can be esily made by
%ensuring that all eigenvalues are positive.
%
%This function is meant as a substitute for Matlabs 'chol' function
%
%Writen by Sami Kanderian, on 07/07/09. Canon U.S. Life Sciences Inc.

[n,m] = size(S);
L=zeros(n,m);

L(1,1)=sqrt(S(1,1));
for i=1:n
    for j=1:n
        if i>j
            sum_1=0;
            if j>1
                for k=1:j-1
                    sum_1=sum_1+L(i,k)*L(j,k);
                end
            end
            L(i,j)=1/L(j,j)*(S(i,j)-sum_1);
        elseif i==j
            sum_2=0;
            if i>1
                for k=1:i-1
                    sum_2=sum_2+L(i,k)*L(i,k);
                end
            end
            L(i,j)=sqrt(S(i,j)-sum_2);
        end
    end
end

if isupper
    T=L';
else
    T=L;
end