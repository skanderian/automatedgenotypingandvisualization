function [r,T] = mymvnrnd(mu,sigma,cases,seed)

[n,d] = size(mu);

% Special case: if mu is a column vector, then use sigma to try
% to interpret it as a row vector.
if d == 1 && size(sigma,1) == n
    mu = mu';
    [n,d] = size(mu);
end

% Single covariance matrix
if ndims(sigma) == 2

    % Factor sigma unless that has already been done, using a function
    % that will perform a Cholesky-like factorization as long as the
    % sigma matrix is positive semi-definite (can have perfect correlation).
    % Cholesky requires a positive definite matrix.  sigma == T'*T

    T=mychol(sigma,1);
    %[T2,err] = cholcov(sigma);
    %[T3,err] = chol(sigma);
    
    r = randn(cases,size(T,1)) * T;
    for i = 1:d
        r(:,i) = r(:,i) + mu(:,i);
    end

end
