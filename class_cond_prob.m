function p=class_cond_prob(x,mu,C)

x=x(:);
mu=mu(:);
dim=length(x);
lnp=-1/2*(x-mu)'*inv(C)*(x-mu)-dim/2*log(2*pi)-1/2*log(det(C));
p=exp(lnp);