function [x,y]=genellipse(ra,rb,ang,x0,y0,Nb)
% [x,y]=genellipse adds ellipses to the current plot
%
% GENELLIPSE(ra,rb,ang,x0,y0,Nb) generates Nb points of an ellipse with semimajor axis of ra,
% a semimajor axis of radius rb, an angle of ang (radians), centered at
% the point x0,y0. Note that if ra=rb, ELLIPSE plots a circle
%
% written by Sami Kanderian. Canon U.S. Life Sciences Inc. May 19, 2009.


Nb=Nb-1;
co=cos(ang);
si=sin(ang);
the=linspace(0,2*pi,Nb(rem(0,size(Nb,1))+1,:)+1)';
x=ra*cos(the)*co-si*rb*sin(the)+x0;
y=ra*cos(the)*si+co*rb*sin(the)+y0;



