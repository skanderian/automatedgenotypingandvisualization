function [ANCKG, SNCKG, meanclass, covclass, all_corr, all_langle]=AI_Training(allcurves, all_kgt, OPT)
%
%OUTPUT DIMENSIONS:
%ANCKG and SNCKG: [<Neqc> x <Nclass>]where Neqc=round((OPT(2)-OPT(1))/OPT(3)+1);
%meanclass: [<Nclass> x <Nclass>]
%covclass:[<Nclass>*<Nclass> x <Nclass>]
%all_corr and all_langle: [<Nmelts> x <Nclass>]
%
%INPUT DIMENSIONS:
%allcurves: [<Neqc> x <Nmelts>] where Neqc=round((OPT(2)-OPT(1))/OPT(3)+1);
%all_kgt: [<Nmelts> x <1>]
%OPT: [<1> x <17>]
%
%This function performs the Artifical Intelligence (machine learning) of
%the training set to which melt curves of unknown genotypes are analyzed against.
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

freq_gt=OPT(14:end); %add two
N_class=sum(freq_gt>0);

pos_gt=zeros(N_class,1);
cnt=0;
for i=1:length(OPT(14:end)) %add two
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
    end
end

[N_r,N_melt]=size(allcurves);
ANCKG=zeros(N_r,N_class);
SNCKG=ANCKG;
ind_all=(1:N_melt);
for j=1:N_class
    ind_class=ind_all(all_kgt==pos_gt(j));
    ANCKG(:,j)=mean(allcurves(:,ind_class),2);
    SNCKG(:,j)=std(allcurves(:,ind_class),0,2);
end

[all_corr, all_langle]=run_curves_against_ANCKG(allcurves, ANCKG);
[meanclass, covclass]=meancovperclass(all_langle, all_kgt, pos_gt); %N_melt x N_class, Nmeltx1