%function [D,ftype]=import_tma_data2(dname_in,fnames_in)
%
%This function is used as part of the Batch Melt Analysis Software 1.0
%It imports melt data from various types of comma separated files
%
%Writen by Sami Kanderian, Canon US Life Sciences, 2008.
%
function [D,F,Ndiec,T_min_data,T_max_data]=import_file_data(dname,fnames_in, exclude_ind, DOPT, KEY)
assay_name_list=strvcat('VKORC1', 'CyP2C9*2', 'CyP2C9*3', 'MTHFR665');
assay_name=deblank(assay_name_list(DOPT(1),:));
T_min_data=Inf;
T_max_data=-Inf;
use_all_wt_as_wc=0;
exclude_ind=[exclude_ind, NaN];
if isempty(assay_name)
    readall=1; %read all columns of data
else
    readall=0;
end
m=0; %all
k=0; %only included indices
Ndiec=zeros(1,5);
for i=1:length(fnames_in)
    F(i).ind_wt=[];
    F(i).ind_he=[];
    F(i).ind_hm=[];
    F(i).ind_wc=[];
    F(i).ind_all=[];
    fname=fnames_in(i).name;
    F(i).fname=fname;
    F(i).fnum=i;

    fid = fopen([dname,fname],'r');
    %Read first 5 characters of file
    tline = fgetl(fid);

    %Read first 5 characters necessary to identify file type
    if strcmpi(tline(1:5), 'Date	') % Experimental CULS filetype
        ftype='CULSexp';
        DAT=importdata([dname,fname],'\t', 1);
        datetm=datenum(cell2mat(DAT.textdata(2:end,2)));
        et=(datetm-datetm(1))*24*60;
        for j=1:8
            k=k+1;
            D(k).fname=fname;
            D(k).time=et;
            D(k).T_raw=DAT.data(:,1);
            D(k).F_raw=DAT.data(:,1+j);
            D(k).Ndat=length(D(k).F_raw);
        end
    elseif strcmpi(tline(1:5), 'Date:') % CULS filetype
        ftype='CULS';
        DAT=importdata([dname,fname],',', 6); %Ignore first 6 header lines

        %read the 8 channels worth of data
        for j=1:8
            k=k+1;
            D(k).fname=fname;
            D(k).fnum=i;
            D(k).time=DAT.data(:,1);
            D(k).T_raw=DAT.data(:,5);
            D(k).F_raw=DAT.data(:,5+j);
            D(k).Ndat=length(D(k).F_raw);
        end
    elseif strcmpi(tline(1:5), '[ EXP') % Caliper filetype'
        ftype='Caliper';
        DAT=importdata([dname,fname],',', 10); %Ignore first 10 header lines
        %       %following line forces key header
        %load('C:\Data\Warfarin_Small_Amplicon\VKORC1_Snaggie\key.mat');
        if ~isempty(KEY)
            DAT.colheaders=KEY;
        end
        %remove possible NaN rows at end of file if NaN exists in any of
        %the colums
        N_data_rows=size(DAT.data,1);
        ind_end=N_data_rows;
        while isnan(sum(DAT.data(ind_end,:)))
            ind_end=ind_end-1;
        end
        %read the 8 channels worth of data
        for j=1:8
            colheader=cell2mat(DAT.colheaders(2*j));

            if strcmpi(colheader(5:end-3),assay_name) || readall%
                m=m+1;
                if m~=exclude_ind(1) %include in data structure
                    k=k+1;

                    D(k).fname=fname;
                    D(k).fnum=i;
                    D(k).header=colheader;
                    D(k).time=DAT.data(1:ind_end,j*2-1);
                    D(k).T_raw=DAT.data(1:ind_end,18);
                    D(k).F_raw=DAT.data(1:ind_end,j*2);
                    D(k).Ndat=length(D(k).F_raw);

                    %location
                    if D(k).header(3)==':'
                        D(k).location=D(k).header(1:2);
                    else
                        D(k).location=D(k).header(1:3);
                    end
                    [D(k).xlocation, D(k).ylocation]=getxylocation(D(k).location);
                    %genotype
                    if strcmpi(colheader(end-1:end),'WT')
                        D(k).kgt=1;
                        D(k).kgtname='WT';
                        F(i).ind_wt=[F(i).ind_wt, k];
                        Ndiec(1)=Ndiec(1)+1;
                    elseif strcmpi(colheader(end-1:end),'HE')
                        D(k).kgt=2;
                        D(k).kgtname='HE';
                        F(i).ind_he=[F(i).ind_he, k];
                        Ndiec(2)=Ndiec(2)+1;
                    elseif strcmpi(colheader(end-1:end),'HM')
                        D(k).kgt=3;
                        D(k).kgtname='HM';
                        F(i).ind_hm=[F(i).ind_hm, k];
                        Ndiec(3)=Ndiec(3)+1;
                    elseif strcmpi(colheader(end-1:end),'WC')
                        if use_all_wt_as_wc
                            D(k).kgt=1;
                            D(k).kgtname='WT';
                            F(i).ind_wt=[F(i).ind_wt, k];
                            Ndiec(1)=Ndiec(1)+1;
                        else
                            D(k).kgt=4;
                            D(k).kgtname='WC';
                            F(i).ind_wc=[F(i).ind_wc, k];
                            Ndiec(4)=Ndiec(4)+1;
                        end
                    elseif strcmpi(colheader(end-1:end),'UK')
                        D(k).kgt=int8(5);
                        D(k).kgtname='UK';
                        Ndiec(5)=Ndiec(5)+1;
                    elseif readall==0
                        error('Invalid genotype identifier');
                    end
                    F(i).ind_all=[F(i).ind_all, k];
                else
                    exclude_ind(1)=[];
                end
            end
        end

    elseif strcmpi(tline(1:5), 'Index') % LCS480 filetype
        ftype='LCS480';
        DAT=importdata([dname,fname],'\t', 1);
        Nj=(size(DAT.data,2)-1)/2;

        %read the Nj channels worth of data
        for j=1:Nj
            colheader=cell2mat(DAT.colheaders(2*j+1));
            if colheader(3)==':';
                sth=5;
            else
                sth=6;
            end
            if ~isempty(findstr(colheader(sth:end-3),assay_name)) || readall% && colheader(end)~='C'
                %j
                k=k+1;
                D(k).fname=fname;
                D(k).fnum=i;
                D(k).header=colheader;
                D(k).T_raw=DAT.data(:,2*j);%+Tshift;
                D(k).F_raw=DAT.data(:,2*j+1);
                D(k).Ndat=length(D(k).F_raw);
                D(k).time=(1:D(k).Ndat)';
                %location
                if D(k).header(3)==':'
                    D(k).location=D(k).header(1:2);
                else
                    D(k).location=D(k).header(1:3);
                end
                [D(k).xlocation, D(k).ylocation]=getxylocation(D(k).location);
                %genotype
                if strcmpi(colheader(end-1:end),'WT')
                    D(k).kgt=int8(1);
                    D(k).kgtname='WT';
                    F(i).ind_wt=[F(i).ind_wt, k];
                    Ndiec(1)=Ndiec(1)+1;
                elseif strcmpi(colheader(end-1:end),'HE')
                    D(k).kgt=int8(2);
                    D(k).kgtname='HE';
                    F(i).ind_he=[F(i).ind_he, k];
                    Ndiec(2)=Ndiec(2)+1;
                elseif strcmpi(colheader(end-1:end),'HM')
                    D(k).kgt=int8(3);
                    D(k).kgtname='HM';
                    F(i).ind_hm=[F(i).ind_hm, k];
                    Ndiec(3)=Ndiec(3)+1;
                elseif strcmpi(colheader(end-1:end),'WC')
                    if use_all_wt_as_wc
                        D(k).kgt=int8(1);
                        D(k).kgtname='WC';
                        F(i).ind_wc=[F(i).ind_wc, k];
                        Ndiec(1)=Ndiec(1)+1;
                    else
                        D(k).kgt=int8(4);
                        D(k).kgtname='WC';
                        F(i).ind_wc=[F(i).ind_wc, k];
                        Ndiec(4)=Ndiec(4)+1;
                    end
                elseif strcmpi(colheader(end-1:end),'UK')
                    D(k).kgt=int8(5);
                    D(k).kgtname='UK';
                    Ndiec(5)=Ndiec(5)+1;
                elseif readall==0
                    %error('Invalid genotype identifier');
                    D(k).kgt=int8(5);
                    D(k).kgtname='UK';
                    Ndiec(5)=Ndiec(5)+1;
                end
                F(i).ind_all=[F(i).ind_all, k];
                T_min_data=min([T_min_data; D(k).T_raw]);
                T_max_data=max([T_max_data; D(k).T_raw]);
            end
        end
    else
        disp(['Could not recognize/read file ',num2str(i), ' of ', num2str(N_files),': ', fname]);
        disp(['First 5 characters must be one of the following:']);
        disp(['     ''Date:'', which is a CULS generated file.']);
        disp(['     ''[ EXP'', which is a Caliper generated file.']);
        disp(['     ''Index'', which is a LCS480 generated file.']);
        %disp(['     ''Raw D'', which is a LCS480 generated file.']);
    end
    fclose(fid);
end
disp('done reading in files');