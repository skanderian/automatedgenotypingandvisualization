%This function performes dimensionality reduction from the 3D correlation
%vector to the 2D vector, and performs Principal Component Analysis to
%obtain eigenvalues and eigenvectors that yield 2D ellipses of 3 standard
%deviations about each genotype
% Written by Sami Kanderian, Canon U.S. Life Sciences Inc. May 21, 2009

function Plot_Points_2D(all_corr2, all_kgt, OPT)

col_mat=[0 0.75 0;
    0 .75 1;
    0.75 0 0;
    0.5 0.5 0.5];

freq_gt=OPT(14:end);
N_class=sum(freq_gt>0);

pos_gt=zeros(N_class,1);
cnt=0;
for i=1:length(OPT(14:end))
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
    end
end

[N_melt]=size(all_corr2,1);
ind_all=(1:N_melt);

hold on;
for j=1:N_class
    ind_class=ind_all(all_kgt==pos_gt(j));
    if ~isempty(ind_class)
        plot(all_corr2(ind_class,1), all_corr2(ind_class,2), '.', 'Color', col_mat(pos_gt(j),:));
    end
end

%plot no calls when necessary
ind_class=ind_all(all_kgt==0);
if ~isempty(ind_class)
    plot(all_corr2(ind_class,1), all_corr2(ind_class,2), '.', 'Color', col_mat(end,:));
end

%plot transformed axis (C,F,E)
maxlp2=max((all_corr2(:,1).^2 + all_corr2(:,2).^2).^0.5);
axes=[0,1,1; 1,1,0; 1,0,1];
transaxes=Dim_Reduction(axes);
maxla2=max((transaxes(:,1).^2 + transaxes(:,2).^2).^0.5);
stransaxes=maxlp2/maxla2*transaxes;
for k=1:3
    plot([0, stransaxes(k,1)], [0, stransaxes(k,2)], 'k');
end

