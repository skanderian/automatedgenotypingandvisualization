function [T_fil, T_filp, T_sortfilp, F_Tsortfilp]= Preprocessing(T_time, T_data, F_time, F_data, Tt_divisor, OPT, FTpaircheck)
%
%OUTPUT DIMENSIONS:
%T_fil:         [<length(T_time)> x <1>]
%T_filp:        [<length(F_time)> x <1>]
%T_sortfilp:    [<length(F_time)> x <1>]
%F_T_sortfilp:  [<length(F_time)> x <1>]
%T_rr:         [<1> x <1>]
%
%INPUT DIMENSIONS:
%Tt_divisor: [<1> x <1>]
%OPT: [<1> x <17>]
%FTpaircheck: : [<1> x <1>] either 0 or 1
%
%This function does some preprocessing of the raw data including filtering
%and sorting of the raw temperature data as well as pairing of temperature to fluorescence data if necessary.
%
%Written by Sami Kanderian on 6/24/2009, Canon U.S. Life Sciences Inc.
%last modified on 04/13/2010

%T_time=(T_time-T_time(1))/Tt_divisor;
T_time=(T_time(:))/Tt_divisor;
dtime_eq=median(diff(T_time),1); % Delta time
dtime_eq=double(dtime_eq(1,1));
%dtime_eq=0.1;
TSG_winsize_sec=OPT(5); %Temperature SG Window Size
TSG_polyord=OPT(6); %Temperature SG Polynomial Order

%Figure out true temperature bounds with extra range for shifting/filtering
%(same for all melts)
TSG_winsize_pts=(2*floor(TSG_winsize_sec/dtime_eq/2))+1; %guaranteed to be odd
TSG_hw_pts=(TSG_winsize_pts-1)/2;

%TSG_winsize_pts[ i1 * TSG_winsize_pts_dim2 + i2] = 1.0 + 2.0 * floor(dtemp_0[ i1 * TSG_winsize_pts_dim2 + i2] / 2.0);
%Create temperature vector with equal spacing necessary for SG analysis (same for all melts):
%F_data_c=F_data(TSG_hw_pts+1:end-TSG_hw_pts);

if TSG_hw_pts>0
    %Calculate SG FIR coefficients (same for all melts)
    TSGFIR_coeff=sav_golay(TSG_polyord, TSG_winsize_pts);  %where ord is an integer and win_pts is an odd integer
    for k=1:TSG_polyord
        TSGFIR_coeff(k+1,:)=TSGFIR_coeff(k+1,:)/(dtime_eq^k);
    end
    %Obtain filtered temperature versus time:
    TSGZDFIR_coeff=(TSGFIR_coeff(1,:))';
    T_fil=myconv(T_data, TSGZDFIR_coeff,1);
    %Added next 2 lines on 
    T_fil(1:TSG_hw_pts)=NaN;
    T_fil(end-TSG_hw_pts+1:end)=NaN;
    %added to force singe dimension:
    T_fil=T_fil(:);
else
    T_fil=T_data;
end

if double(FTpaircheck)>0.5 %add 0.5 to take care of integer comparison when ==1
    %check for duplicate T_time data (done above) %04/12/2010
    %Interpolate temperatures (T_fil) to match up with Flourescence time:
    T_filp=interp1(T_time,T_fil,F_time); 
    %remove trailing NaNs %04/12/2010
    T_filp=T_filp(:);
else
    T_filp=T_fil;
end

[T_sortfilp, ind]=sort(T_filp,1);
T_sortfilp=T_sortfilp(:); %for agility purposes
F_Tsortfilp=F_data(ind,1);
%Next three lines are very important to handle repeated x (temperature) points
ind=find(diff(T_sortfilp)==0);
N_rep=length(ind);
if N_rep>0
    pad=NaN*ones(N_rep,1);
    T_sortfilp(ind)=[];
    F_Tsortfilp(ind)=[];
    T_sortfilp=[T_sortfilp; pad];
    F_Tsortfilp=[F_Tsortfilp; pad];
end