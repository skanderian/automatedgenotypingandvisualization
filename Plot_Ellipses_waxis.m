function Plot_Ellipses_waxis(ellipse_x, ellipse_y, OPT, Nppe, axes_x, axes_y)

col_mat=[0 0.75 0;
    0 .75 1;
    0.75 0 0;
    1 0 1];

freq_gt=OPT(14:end);
N_class=sum(freq_gt>0);

pos_gt=zeros(N_class,1);
cnt=0;
for i=1:length(OPT(14:end))
    if freq_gt(i)>0
        cnt=cnt+1;
        pos_gt(cnt)=i;
    end
end
hold on;
for j=1:N_class
    mlstind=(j-1)*Nppe+1;
    mledind=j*Nppe;
    x=ellipse_x(mlstind:mledind);
    y=ellipse_y(mlstind:mledind);
    plot([x(1:end);x(1)],[y(1:end);y(1)],'Color', col_mat(pos_gt(j),:), 'LineWidth', 1);
end

for j=1:N_class
    amlstind=(j-1)*4+1;
    amledind=j*4;
    ax=axes_x(amlstind:amledind);
    ay=axes_y(amlstind:amledind);
    plot([ax(1);ax(3)],[ay(1);ay(3)],'Color', [0,0,0], 'LineWidth', 1, 'LineStyle', ':');
    plot([ax(2);ax(4)],[ay(2);ay(4)],'Color', [0,0,0], 'LineWidth', 1, 'LineStyle', ':');
end