%this function plots the 3D data pooint onto a 3D graph. It enables the
%vizualization of the spread of clusters amongst different genotypes
%
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

function plot_distribution(fig, prefix, all_gt, all_corr, all_langle, DOPT)
assay_list=strvcat('VKORC1','CyP2C9*2','CyP2C9*3','MTHFR665');
assay_num=DOPT(1);
assay_name=assay_list(assay_num,:);

col_mat=[0 0.75 0;
    0 .75 1;
    0.75 0 0;
    1 0 1];

figure(fig);

subplot(211);
classbound_corr_3(all_corr, all_gt, assay_name, col_mat);
title([prefix, assay_name, ' Normalized parameter distribution in 3D space']);
view(135,35.25); axis equal;
subplot(212);
classbound_langle_3(all_langle, all_gt, assay_name, col_mat);
view(111,11.25); axis equal;
orient tall;