function w=myconv(a,b,sr)
% This is a function that performs convolution of two vecrors a & b for
% filtering. It is similar to the Matlab built in function 'conv'
% but is compilable to Cwith the Agolity MCS tool
%
%if sr==0, full length is returned 
%if sr==1, same as conv(a,b,'same')
%if sr==2, same as conv(a,b,'valid')
%
%
% written by Sami Kanderian on 12/18/2008. Canon U.S. Life Sciences Inc.

% ** mbfunctions for input arguments
%mbrealcol(a)
%mbrealcol(b)
%mbintscalar(sr)

la=length(a);
lb=length(b);

if la>lb
    u=a;
    v=b;
    m=la;
    n=lb;
else
    u=b;
    v=a;
    m=lb;
    n=la;
end
%u is the larger,
%v is the smaller
%m=length(u);
%n=length(v);
hn=floor(n/2);
w=zeros(m+n-1,1);
for k=1:m+n-1
    sum=0;
    for j = max(1,k+1-n): min(k,m)
        sum=sum+u(j)*v(k+1-j);
    end
    w(k)=sum;
end
if sr==1
    w=w(hn+1:m+hn);
elseif sr==2
    w=w(n:m);
end
