function imagescbord(Cmat, Gvec_kgt, Gvec_agt, unique_kgt)

%function [hi, hh, hv]=imagescbord(Cmat,D,varargin)
Cmat=Cmat';
classname=['WT';'HE';'HM';'UK'];

hi=imagesc(Cmat);
hold on;

[Ni,Nj]=size(Cmat);
border_x=(0:1:Nj)+0.5;
border_y=(0:1:Ni)+0.5;

%plot horizontal borders
hh=zeros(Ni+1,1);
hv=zeros(Nj+1,1);
for i=1:Ni+1
    hh(i)=plot([border_x(1),border_x(end)], [border_y(i), border_y(i)],'k','Color', [0,0,0]);
end
for j=1:Nj+1
    hv(j)=plot([border_x(j),border_x(j)], [border_y(1), border_y(end)],'k','Color', [0,0,0]);
end

% if ~isempty(varargin)
%     Gvec_kgt=varargin{1};
% else
%     Gvec_kgt=5*ones(Ni,1);
% end

for i=1:Ni %genotypes
    for j=1:Nj %melts
        if i==1 && Gvec_kgt(j)~=5
            if Gvec_kgt(j)==4;
                Gvec_kgt(j)=1;
            end
            text(j,double(Gvec_kgt(j)), 'O','Color', [1,1,1],'HorizontalAlignment','center','FontSize', 10); %40
        end
        if Cmat(i,j)==max(Cmat(:,j)) && Gvec_agt(j)~=0
            text(j,i, 'X','Color', [1,1,1],'HorizontalAlignment','center','FontSize', 8); %12
            %D(j).gtcall=classname(i,:);
        end
    end
end
colormap(flipud(colormap(bone)));

% if expt~=0
%     if label==1
%         set(gca,'xtick', (1:12))
%         set(gca,'xticklabel', [1,2,3,4,5,6,1,2,3,4,5,6]);
%     elseif label==2
%         set(gca,'xtick', (1:36))
%         xtl=[0,1,2,3,4,5];
%         xtl(2:end)=xtl(2:end)+(expt-1)*5;
%         xtl=[xtl,xtl,xtl,xtl,xtl];
%         set(gca,'xticklabel', xtl);
%     end
% end