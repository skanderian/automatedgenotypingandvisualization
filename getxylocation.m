function [xlocation, ylocation]=getxylocation(location)

xlocation=NaN;
ylocation=NaN;
if length(location)>1
    location_letter=location(1);
    xlocation=int8(str2double(location(2:end)));
    switch upper(location_letter)
        case 'A'
            ylocation=1;
        case 'B'
            ylocation=2;
        case 'C'
            ylocation=3;
        case 'D'
            ylocation=4;
        case 'E'
            ylocation=5;
        case 'F'
            ylocation=6;
        case 'G'
            ylocation=7;
        case 'H'
            ylocation=8;
        otherwise
            ylocation=NaN;
    end
end
