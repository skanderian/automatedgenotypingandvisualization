function [all_agt, ppost_mat, pccd_mat]=Call_Genotype(all_corr, all_langle, meanclass, covclass, OPT)
%
%OUTPUT DIMENSIONS:
%all_agt: [<Nmelts> x <1>] where Nmelts is the number of melts being analyzed
%ppost_mat and pccd_mat: [<Nmelts> x <Nclass>]. 
%
%INPUT DIMENSIONS:
%all_corr and all_langle: [<Nmelts> x <Nclass>] where Nclass is the number
%of possible genotypes for the assay
%meanclass: [<Nclass> x <Nclass>]
%covclass: [<Nclass>*<Nclass> x <Nclass>]
%OPT: [<1> x <15>]
%
%This function makes the call of an unknown genotypes are analyzed against
%a previously trained set of known genotype melt curves.
%It calculates probabilities and checks to see which and if the genotype 
%with the largest posterior probability satisfies certain thresholds, 
%otherwise a no call is given
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

ppost_thresh=OPT(9); %add 2
mincorr_thresh=OPT(10); %add 2
%percwic_thresh=OPT(9); %add 2

freq_gt=OPT(14:end); %add 2
gt_ind=(1:length(OPT(14:end)));

nzf=(freq_gt~=0);
freq_gt=freq_gt(nzf);
gt_ind=gt_ind(nzf);

[ppost_mat, pccd_mat]=calcprobability(all_langle, freq_gt, meanclass, covclass);
%percwic_mat=ones(size(ppost_mat));

N_melt=size(all_langle,1);
all_agt=zeros(N_melt,1);
for i=1:N_melt
    ppost_vec=ppost_mat(i,:);
    corr_vec=all_corr(i,:);
    if ~isnan(max(ppost_vec))
        ind_max_ppost=(ppost_vec==max(ppost_vec));
        ppost=ppost_vec(ind_max_ppost);
        ppost=ppost(1);
        %modified line below on 5/28/09
        corr_maxppost=corr_vec(ind_max_ppost);
        corr_maxppost=corr_maxppost(1);
        %percwic=percwic(1);
        agt=gt_ind(ind_max_ppost);
        agt=agt(1);
        if (ppost>=ppost_thresh && corr_maxppost>=mincorr_thresh)
            all_agt(i)=agt;
        end
    end
end
