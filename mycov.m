function C=mycov(X)
%This is my substitute for matlabs covariance function (cov).
% written by Sami Kanderian on 12/17/08, Canon U.S. Life Sciences Inc.
[Nr, Nc]=size(X);
%since the covariance matrix is symmetric, the number of unique elements is (Nc*Nc+1)/2;
C=zeros(Nc,Nc);
mnX=mean(X); %mean of each column (only has to be computed once)
for i=1:Nc
    for j=i:Nc
        if Nr > 1 %added check for more than 1 row on Aug 13, 2009
            covij=sum((X(:,i)-mnX(i)).*(X(:,j)-mnX(j)))/(Nr-1);
            C(i,j)=covij;
            C(j,i)=covij;
        end
    end
end
