function [ellipse_x, ellipse_y, percontainpop, axes_x, axes_y]=Generate_Ellipses_2D_waxis(meanclass2, maxeval2, mineval2, ang2, Nppe, numstdev, OPT)
%
%This function generates one or more ellipses based on means, eigenvalues,
%and angles obtained from eigenvectors obtained from training set, and desired standard deviation.
%written by Sami Kanderian, Canon U.S. Life Sciences Inc. May 27, 2009.
%
%OUTPUT DIMENSIONS:
%ellipse_x: [<Nmelts> x <1>]
%ellipse_y: [<Nmelts> x <1>]
%percontainpop: [<1> x <1>]
%
%INPUT DIMENSIONS:
%meanclass2: [<2> x <3>]
%maxeval2:[<3> x <1>]
%mineval2:[<3> x <1>]
%ang2:[<3> x <1>]
%Nppe:[<1> x<1>]
%numstdev:[<1> x<1>]
%OPT: [<1> x <15>]

freq_gt=OPT(14:end);
N_class=sum(freq_gt>0);
ellipse_x=zeros(Nppe*N_class,1);
ellipse_y=ellipse_x;
axes_x=zeros(4*N_class,1);
axes_y=axes_x;
for j=1:N_class
    mlstind=(j-1)*Nppe+1;
    mledind=j*Nppe;
    amlstind=(j-1)*4+1;
    amledind=j*4;
    
    M=meanclass2(:,j);
    ra=numstdev*sqrt(maxeval2(j));
    rb=numstdev*sqrt(mineval2(j));
    ang=ang2(j);
    [x,y]=genellipse(ra,rb,ang,M(1),M(2),Nppe);
    [ax,ay]=genellipse(ra,rb,ang,M(1),M(2),5);
    ellipse_x(mlstind:mledind)=x;
    ellipse_y(mlstind:mledind)=y;
    axes_x(amlstind:amledind)=ax(1:4);
    axes_y(amlstind:amledind)=ay(1:4);
end

stdvec=(0:0.1:5)';

pwe=[0
   0.499880000000000
   1.972280000000000
   4.398420000000000
   7.679960000000000
  11.742150000000001
  16.452690000000000
  21.708540000000003
  27.373249999999999
  33.295200000000001
  39.347020000000001
  45.391700000000000
  51.315920000000006
  57.033480000000004
  62.453789999999998
  67.516459999999995
  72.181879999999992
  76.416579999999996
  80.212419999999995
  83.556980000000010
  86.478229999999996
  88.983840000000001
  91.114120000000000
  92.904480000000007
  94.397329999999997
  95.617800000000003
  96.604569999999995
  97.395769999999999
  98.022670000000005
  98.510829999999999
  98.892510000000001
  99.183229999999995
  99.404749999999993
  99.567160000000001
  99.691519999999997
  99.781850000000006
  99.847409999999996
  99.894680000000008
  99.927779999999998
  99.950850000000003
  99.966949999999997
  99.978070000000002
  99.985820000000004
  99.990749999999991
  99.993829999999988
  99.995999999999995
  99.997590000000002
  99.998570000000001
  99.999030000000005
  99.999349999999993
  99.999590000000012];

percontainpop=interp1(stdvec,pwe,numstdev);