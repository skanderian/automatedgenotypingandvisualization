%This function converts a set of points (where each row represents a data
%point) into shperical coordinates. This forces the non normal correlation
%coefficient distribution into a new space that is normally distributed in
%all parameters
%
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

function LAmat=langle(Cmat)
[Nsamp, Ndim]=size(Cmat);
LAmat=zeros(Nsamp, Ndim);

if Ndim==2
    for k=1:Nsamp
        Cvec=Cmat(k,:);
        LAmat(k,1)=sqrt(Cvec*Cvec');
        LAmat(k,2)=atan(Cvec(2)/Cvec(1));
    end
elseif Ndim==3
    for k=1:Nsamp
        Cvec=Cmat(k,:);
        LAmat(k,1)=sqrt(Cvec*Cvec');
        LAmat(k,2)=atan(Cvec(2)/Cvec(1));
        LAmat(k,3)=atan(Cvec(3)/sqrt(Cvec(1)^2+Cvec(2)^2));
    end
else %Ndim==4;
    for k=1:Nsamp
        Cvec=Cmat(k,:);
        LAmat(k,1)=sqrt(Cvec*Cvec');
        LAmat(k,2)=atan(Cvec(2)/Cvec(1));
        LAmat(k,3)=atan(Cvec(3)/sqrt(Cvec(1)^2+Cvec(2)^2));
        LAmat(k,4)=atan(Cvec(4)/sqrt(Cvec(1)^2+Cvec(2)^2+Cvec(3)^2));
    end
end

