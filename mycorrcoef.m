% This is a function that performs correlation between two vecrors x & y for
% filtering. It is similar to the Matlab built in function 'corrcoef'
% but is compilable to C with the Agolity MCS tool
%
% written by Sami Kanderian on 12/18/2008. Canon U.S. Life Sciences Inc.

function cc=mycorrcoef(x,y)
N=length(x);
mx=mean(x);
my=mean(y);
%covxx=sum((x-mx).*(x-mx))/(N-1);
%covyy=sum((y-my).*(y-my))/(N-1);
covxy=sum((x-mx).*(y-my))/(N-1);
stdx=std(x);
stdy=std(y);
cc=covxy/(stdx*stdy);