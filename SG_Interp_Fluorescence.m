function [F_eqa, neg_dFdT_eqb]= SG_Interp_Fluorescence(T_sortfil, F_Tsortfil, T_eqa, SGFIR_coeff)
%
%OUTPUT DIMENSIONS:
%F_eqa: : [<Neqa> x <1>] where Neqa=(T_max-T_min+2*Tshift_max)/dT_eq+(2*floor(SG_winsize_deg/dT_eq/2))+1;
%neg_dFdT_eqb: [<Neqb> x <1>] where Neqb=((T_max-T_min+2*Tshift_max)/dT_eq+1)
%
%INPUT DIMENSIONS:
%T_eqa: [<Neqa> x <1>] where Neqa=(T_max-T_min+2*Tshift_max)/dT_eq+(2*floor(SG_winsize_deg/dT_eq/2))+1;
%SGFIR_coeff: [<SG_polyord+1> x <2*floor(SG_winsize_deg/dT_eq/2))+1>]
%OPT: [<1> x <17>]
%
%This function obtains intepolated fluorescence and the negative derivatives of fluorescence wrt
%temperature of an individual melt curve.
% written by Sami Kanderian Canon US LifeSciences. Last modified 12/05/2008

%Create interpolated fluoresence vector necessary for SG analysis (output for debugging purposes if necessary):
F_eqa=FTinterp(T_sortfil, F_Tsortfil, T_eqa, 0);

%Obtain negative derivative of fluoresence wrt temperature:
SGFDFIR_coeff=(SGFIR_coeff(2,:))';
%neg_dFdT_eqb=-conv2(F_eqa, SGFDFIR_coeff, 'same');    %first derivative
%neg_dFdT_eqb=neg_dFdT_eqb(SG_hw_pts+1:end-SG_hw_pts);
%Following line is the same as the above 2 lines
%neg_dFdT_eqb=-conv2(F_eqa, SGFDFIR_coeff, 'valid');
neg_dFdT_eqb=-myconv(F_eqa, SGFDFIR_coeff,2);

%replace with non-derivative
%neg_dFdT_eqb=myconv(F_eqa, ones(size(SGFDFIR_coeff)),2)/length(SGFDFIR_coeff);
%replace with second derivative
%SGSDFIR_coeff=(SGFIR_coeff(3,:))';
%neg_dFdT_eqb=-myconv(F_eqa, SGSDFIR_coeff,2);

%added to force single dimension:
neg_dFdT_eqb=neg_dFdT_eqb(:);