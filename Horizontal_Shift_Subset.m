function [T_eqc, refcurve, hscurvemat, tempshiftvec]=Horizontal_Shift_Subset(temperature, curvemat, expno, kgt, OPT, inrefcurve)
%
%OUPUT DIMENSIONS
%T_eqc: [<Neqc> x <1>] where Neqc=round((OPT(2)-OPT(1))/OPT(3)+1);
%refcurve: [<Neqc> x <1>] (same as T_eqc)
%hscurvemat: [<Neqc> x <Nmelts>] where Nmelts is the number of melts being analyzed
%tempshift: [<Nmelts> x <1>]
%
%INPUT DIMENSIONS:
%temperature: [<Neqa> x <1>]
%curvemat: [<Neqb> x <Nmelt>] where Neqb=((T_max-T_min+2*Tshift_max)/dT_eq+1)
%expno: [<Nmelt> x <1>]
%kgt: [<Nmelt> x <1>]
%OPT: [<1> x <17>]
%inrefcurve: [<Neqc> x <1>]
%
%This function makes does horizontal shifting of all curves in 'curvemat'
%using corresponding positive controls that are shifted to line up with
%'inrefcurve'. if 'inrefcurve' is a zero vector, than the reference curve
%is derived from experiment 1 positive contols which is shifted to give the
%average shift of the training set of 0.
%
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

if sum(inrefcurve==0)==length(inrefcurve) %all zeros
    istraining=1;
    ssi=2;
else
    istraining=0;
    ssi=1;
end

%Find window of T_min T_max of interest (specified by user)with +/shift buffer
Temp_min_agbf=OPT(1)-OPT(4);
Temp_max_agbf=OPT(2)+OPT(4);
dT=OPT(3);
usehshift=OPT(12);  %add 2
pc=OPT(13); %add 2
Temp_min_dr=temperature(1);

ind_sta=round((Temp_min_agbf-Temp_min_dr)/dT+1);
ind_eda=round((Temp_max_agbf-Temp_min_agbf)/dT+ind_sta);
[N_t, N_melt]=size(curvemat);

if usehshift==1

    %take subset of window of interest
    curvemat_ir=curvemat(ind_sta:ind_eda,:);
    unqexp=unique(expno);
    N_exp=length(unqexp);
    N_wbuff=(ind_eda-ind_sta)+1;
    avenormpcinexp=zeros(N_wbuff, N_exp);

    %Cycle through each experiment, grab positive controls, normalize them
    %and take the average. avenormpcinexp(:,i) is the average normalized curve
    %of all the positive controls in experiment (i)
    for i=1:N_exp
        ind=find(expno==unqexp(i) & kgt==pc);
        %Normalized positive controls in experiment i
        normpcinexp=normavestd(curvemat_ir(:,ind));
        avenormpcinexp(:,i)=mean(normpcinexp,2);
    end

    %Find window of T_min T_max of interest (specified by user)without +/shift buffer
    ind_stb=round(OPT(4)/dT+1);
    ind_edb=round(N_wbuff-OPT(4)/dT);
    if istraining
        %use the average normalized positive control curve of the first experiment
        %as a reference for shifting
        refcurve=avenormpcinexp(ind_stb:ind_edb,1);
    else %use reference curve input
        refcurve=inrefcurve;
    end

    maxlshiftind=ind_stb-1;
    maxrshiftind=N_wbuff-ind_edb;
    N_shifts=maxlshiftind+maxrshiftind+1;
    shiftindexp=zeros(1,N_exp);
    %if N_exp>1
    if (N_exp>1 && istraining) || istraining==0
        for i=ssi:N_exp %loop through experiments
            corrmax=-1;
            corrvec=-1*ones(N_shifts,1);
            cnt=0;
            for j=-maxlshiftind:maxrshiftind
                cnt=cnt+1;
                corrvec(cnt)=mycorrcoef(avenormpcinexp(ind_stb-j:ind_edb-j,i), refcurve);
                if corrvec(cnt)>corrmax;
                    shiftindexp(i)=j;
                    corrmax=corrvec(cnt);
                end
            end
        end
    end
    %if istraining %added && 0
    if istraining && 0 %added && 0
        %Force average shift of all experiments to zero if training set;
        romesh=round(mean(shiftindexp));
        shiftindexp=shiftindexp-romesh;%
        refcurve=avenormpcinexp(ind_stb-romesh:ind_edb-romesh,1);
    end

    %shift each melt curve according to the positive control of the
    %corresponding experiment
    hscurvemat=NaN*curvemat;
    shiftindmelt=zeros(N_melt,1);
    for k=1:N_melt        
        shiftindmelt(k)=shiftindexp(expno(k));
        if shiftindmelt(k)==0
            hscurvemat(:,k)=curvemat(:,k);
        elseif shiftindmelt(k)<0 %when shiftindmelt is negative we have to shift back relative to the reference
            indstc=1;
            indedc=N_t+shiftindmelt(k);
            hscurvemat(indstc:indedc,k)=curvemat(indstc-shiftindmelt(k):indedc-shiftindmelt(k),k);
        else %if shiftindmelt(k)>0 %when shiftindmelt is positive we have to shift forward relative to the reference
            indstc=1+shiftindmelt(k);
            indedc=N_t;
            hscurvemat(indstc:indedc,k)=curvemat(indstc-shiftindmelt(k):indedc-shiftindmelt(k),k);
        end
    end
else
    hscurvemat=curvemat;
    refcurve=inrefcurve;
    shiftindmelt=zeros(N_melt,1);
end

%calculate temperature shift from shiftindmelt and dT
tempshiftvec=shiftindmelt*dT;

%include only region of interest
Temp_min_ag=OPT(1);
Temp_max_ag=OPT(2);

ind_std=round((Temp_min_ag-Temp_min_dr)/dT+1);
ind_edd=round((Temp_max_ag-Temp_min_agbf)/dT+ind_sta);

hscurvemat=hscurvemat(ind_std:ind_edd,:);
%T_eqc=(OPT(1):OPT(3):OPT(2))';
%previous line sometimes has rounding issue. Replaced with following lines on 100110
N_eqc=ind_edd-ind_std+1;
T_eqc=(0:N_eqc-1)'*OPT(3)+OPT(1);