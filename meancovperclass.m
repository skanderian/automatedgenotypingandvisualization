function [MEANCLASS, COVCLASS]=meancovperclass(Param_mat,Class_ind,UClass_ind) 
%This function calculates the mean and covariance of a  training set of
%parameters for each class. These distributions are  used to classify a
%parameter set(data point).
%
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.
%UClass_ind=unique(Class_ind);
[N_melt,N_params]=size(Param_mat);
N_class=N_params;
UClass_ind=UClass_ind(1:N_class);
MEANCLASS=zeros(N_params, N_class);
COVCLASS=zeros(N_params*N_class, N_params);

for k=1:length(UClass_ind);
    ind_class=(Class_ind==UClass_ind(k));
    Param_mat_inclass=Param_mat(ind_class,:);
    MEANCLASS(:,k)=(mean(Param_mat_inclass,1))';
    row_st=N_class*(k-1)+1;
    row_ed=N_class*k;
    COVCLASS(row_st:row_ed,:)=mycov(Param_mat_inclass);
end

