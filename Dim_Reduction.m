function all_corr2=Dim_Reduction(all_corr)
%This function does dimensionality reduction from 3D to 2D via projection
%of the 3D points to the plane normal to the (1,1,1) vector
%
%Written by Sami Kanderian on May 21, 2009
%
%OUTPUT DIMENSIONS:
%all_corr2: [<Nmelts> x <2>]
%
%INPUT DIMENSIONS:
%all_corr: [<Nmelts> x <3>]

% ** mbfunctions for input arguments
%mbrealmatrix(all_corr)

T=[-1, -1/sqrt(3); 1, -1/sqrt(3); 0, 2/sqrt(3)];

%Dimensionality Reduction from 3D to 2D
all_corr2=all_corr*T;
