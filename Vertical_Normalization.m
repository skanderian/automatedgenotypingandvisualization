function vscurvemat=Vertical_Normalization(curvemat)
%
%OUTPUT DIMENSIONS:
%curvemat [<Neqc> x <Nmelts>] where Neqc=round((OPT(2)-OPT(1))/OPT(3)+1);
%
%INPUT DIMENSIONS:
%vscurvemat [<Neqc> x <Nmelts>]
%
%This function performs vertical normalization of an array of curves (in
%columns). Each curve is normalizes to have a mean of 0 and a standard
%deviation of 1
% written by Sami Kanderian on 12/8/2008. Canon U.S. Life Sciences Inc.

[N_r,N_melt]=size(curvemat);
vscurvemat=zeros(N_r,N_melt);
for k=1:N_melt
    vscurvemat(:,k)=normavestd(curvemat(:,k)); %Normalize to have 0 mean and unit standard deviation    
end