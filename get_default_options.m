%This function reads in user options from a csv file that will be used for
%melt analysis ang genotype determination
%Sami Kanderian,    Canon U.S. Life Sciences Inc.

function OPT=get_default_options(opt_dfname, assay_num)
OPT=csvread(opt_dfname,1,1);
OPT=OPT(assay_num,:);