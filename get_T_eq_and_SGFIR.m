function [T_eqa, T_eqb, SGFIR_coeff]=get_T_eq_and_SGFIR(OPT, T_min_data, T_max_data)
%
%OUTPUT DIMENSIONS:
%T_eqa: [<Neqa> x <1>] where Neqa=(T_max-T_min+2*Tshift_max)/dT_eq+(2*floor(SG_winsize_deg/dT_eq/2))+1;
%substitute with
%T_eqa: [<Neqa> x <1>] where Neqa=(T_max_down-T_min_up)/dT_eq+1;
%where T_min_up=ceil(T_min_data/dT_eq)*dT_eq;
%and T_max_down=floor(T_max_data/dT_eq)*dT_eq
%SGFIR_coeff: [<SG_polyord+1> x <2*floor(SG_winsize_deg/dT_eq/2))+1>]
%T_eqb: [<Neqb> x <1>] where Neqb=(Neqa-SGFIR_coeff(2)+1)
%
%INPUT DIMENSIONS:
%OPT: [<1> x <17>]
%T_min_data: [<1> x <1>]
%T_max_data: [<1> x <1>]
%
%This function obtains two standard temperature vectors and a set to
%Savitzky-Golay filter coefficients based on input user options
% written by Sami Kanderian on 11/08, Canon U.S. Life Sciences

%Remember Matlab indexing starts with 1 and C indexing starts with 0
dT_eq=OPT(3); % Delta Temperature
%Tshift_max=OPT(4); %Maximum temperature shift
SG_winsize_deg=OPT(7); %SG Window Size
SG_polyord=OPT(8); %SG Polynomial Order
T_min_up=ceil(T_min_data/dT_eq)*dT_eq;
T_max_down=floor(T_max_data/dT_eq)*dT_eq;

%Figure out true temperature bounds with extra range for shifting/filtering
%(same for all melts)
SG_winsize_pts=(2*floor(SG_winsize_deg/dT_eq/2))+1; %guaranteed to be odd
SG_hw_pts=(SG_winsize_pts-1)/2;

%T_eq_st=T_min-Tshift_max-SG_hw_pts*dT_eq;
%T_eq_ed=T_max+Tshift_max+SG_hw_pts*dT_eq;
T_eq_st=T_min_up;
T_eq_ed=T_max_down;

%Create temperature vector with equal spacing necessary for SG analysis (same for all melts):
T_eqa=(T_eq_st: dT_eq: T_eq_ed)';
T_eqb=T_eqa(SG_hw_pts+1:end-SG_hw_pts);
%Calculate SG FIR coefficients (same for all melts)
SGFIR_coeff=sav_golay(SG_polyord, SG_winsize_pts);  %where ord is an integer and win_pts is an odd integer
for k=1:SG_polyord
    SGFIR_coeff(k+1,:)=SGFIR_coeff(k+1,:)/(dT_eq^k);
end

